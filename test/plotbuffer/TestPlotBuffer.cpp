#include "TestPlotBuffer.h"
#include "plotbuffer.h"

#define MEMCPY_INC_POINTER(value)                                              \
  memcpy(pointer, &value, sizeof(value));                                      \
  pointer += sizeof(value);

#define COMPARE(getterFunction, expected_value)                                \
  {                                                                            \
    auto value = PlotBuffers::get##getterFunction(position, data);             \
    QCOMPARE(value, expected_value);                                           \
    position += sizeof(expected_value);                                        \
  }

void TestPlotBuffer::TestGet() {
  char data[sizeof(double) + sizeof(float) + sizeof(int16_t) +
            sizeof(uint16_t) + sizeof(char) + sizeof(uint8_t) + sizeof(bool) +
            sizeof(int32_t) + sizeof(uint32_t) + sizeof(int64_t) +
            sizeof(uint64_t)];

  const double value_double = 83249.234;
  const float value_float = 28398.2389;
  const int16_t value_int16_t = 32292;
  const uint16_t value_uint16_t = 60282;
  const char value_char = 'A';
  const uint8_t value_uint8_t = 182;
  const bool value_bool = true;
  const int32_t value_int = 129803823;
  const uint32_t value_uint32_t = 2098349023;
  const int64_t value_int64_t = 892739487394857;
  const uint64_t value_uint64_t = 3849283472938472923;

  char* pointer = data;
  MEMCPY_INC_POINTER(value_double);
  MEMCPY_INC_POINTER(value_float);
  MEMCPY_INC_POINTER(value_int16_t);
  MEMCPY_INC_POINTER(value_uint16_t);
  MEMCPY_INC_POINTER(value_char);
  MEMCPY_INC_POINTER(value_uint8_t);
  MEMCPY_INC_POINTER(value_bool);
  MEMCPY_INC_POINTER(value_int);
  MEMCPY_INC_POINTER(value_uint32_t);
  MEMCPY_INC_POINTER(value_int64_t);
  MEMCPY_INC_POINTER(value_uint64_t);

  int position = 0;
  COMPARE(Double, value_double);
  COMPARE(Float, value_float);
  COMPARE(Short, value_int16_t);
  COMPARE(UShort, value_uint16_t);
  COMPARE(Char, value_char);
  COMPARE(UChar, value_uint8_t);
  COMPARE(Bool, value_bool);
  COMPARE(Int, value_int);
  COMPARE(UInt, value_uint32_t);
  COMPARE(Int64, value_int64_t);
  COMPARE(UInt64, value_uint64_t);

  // Check that no check is missing
  QCOMPARE(position, sizeof(data));
}

QTEST_MAIN(TestPlotBuffer);
