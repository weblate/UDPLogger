#ifndef TESTPLOTBUFFER_H
#define TESTPLOTBUFFER_H

#include "CommonTest.h"

class TestPlotBuffer : public CommonTest {
  Q_OBJECT

private slots:
  void TestGet();
};

#endif // TESTPLOTBUFFER_H
