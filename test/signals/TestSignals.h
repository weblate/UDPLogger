#ifndef TESTSIGNALS_H
#define TESTSIGNALS_H

#include "CommonTest.h"

class TestSignals : public CommonTest {

public slots:
  void TestSetSignals();
};

#endif // TESTSIGNALS_H
