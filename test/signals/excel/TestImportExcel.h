#ifndef TESTIMPORTEXCEL_H
#define TESTIMPORTEXCEL_H

#include "CommonTest.h"

class TestImportExcel : public CommonTest {
  Q_OBJECT

private slots:
  void Test1();
  void TestInvalidExcel();
  void TestHeaderNotFound();
};

#endif // TESTIMPORTEXCEL_H
