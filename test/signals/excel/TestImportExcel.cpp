#include "TestImportExcel.h"
#include "signals.h"

#include <QRegularExpression>

#ifndef HAVE_XLNT
#error XLNT Required
#endif

void TestImportExcel::Test1() {
  const QString path = QFINDTESTDATA("data/UDPLoggingAttitudeEstimation.xlsx");

  QVector<Signal> refSignals;
  refSignals.append({
      .name = "time",
      .datatype = "int32_t",
      .unit = "ms",
      .struct_name = "",
  });
  refSignals.append({.name = "input_bus.mpu6050data.temp",
                     .datatype = "float",
                     .unit = "°C",
                     .struct_name = "MultikopterInput"});
  refSignals.append({.name = "input_bus.mpu6050data.gyx",
                     .datatype = "float",
                     .unit = "°/s",
                     .struct_name = "MultikopterInput"});
  // At least the name, index and the datatype is needed
  refSignals.append({.name = "input_bus.mpu6050data.gyy",
                     .datatype = "float",
                     .unit = "",
                     .struct_name = ""});

  Signals signals_;
  QCOMPARE(signals_.importXLSX(path), 0);

  QCOMPARE(signals_.getSignalCount(), 4);

  for (int i = 0; i < signals_.getSignalCount(); i++) {
    QCOMPARE(signals_.getSignal(i).index, i);
    QCOMPARE(signals_.getSignal(i).name, refSignals.at(i).name);
    QCOMPARE(signals_.getSignal(i).struct_name, refSignals.at(i).struct_name);
    QCOMPARE(signals_.getSignal(i).unit, refSignals.at(i).unit);
    QCOMPARE(signals_.getSignal(i).datatype, refSignals.at(i).datatype);
    QCOMPARE(signals_.getSignal(i).startByte,
             i * 4); // int32, float, float, float, float
  }
}

void TestImportExcel::TestHeaderNotFound() {
  const QString path = QFINDTESTDATA("data/HeaderNotFound.xlsx");

  Signals signals_;
  QCOMPARE(signals_.importXLSX(path), -1); // Just check that not crash
}

void TestImportExcel::TestInvalidExcel() {
  const QString path = QFINDTESTDATA("data/Invalid.xlsx");

  Signals signals_;

  connect(&signals_, &Signals::invalidSignal, this, [] {
    static int counter = 0;
    counter++;

    QString s;
    switch (counter) {
      case 1:
        s = tr("Signal %1: Name is invalid").arg("2");
        break;
      case 2:
        s = tr("Signal %1: Datatype is invalid").arg("3");
        break;
      case 3:
        s = tr("Signal %1: Datatype is invalid").arg("4");
        break;
      default:
        QVERIFY(false);
    }
  });

  QCOMPARE(signals_.importXLSX(path), 0);
}

QTEST_MAIN(TestImportExcel);
