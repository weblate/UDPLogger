#ifndef TESTSIGNALSETTINGS_H
#define TESTSIGNALSETTINGS_H

#include "CommonTest.h"

class TestSignalSettings : public CommonTest {
  Q_OBJECT

private slots:
  void TestImportExport();
  void TestSignalImportExport();
  void TestSignalImportExportInvalidDatatype();
};

#endif // TESTSIGNALSETTINGS_H
