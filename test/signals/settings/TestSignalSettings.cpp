#include "TestSignalSettings.h"
#include "signals.h"

#include <QJsonObject>

void TestSignalSettings::TestImportExport() {

  QVector<Signal> refSignals;
  refSignals.append({
      .name = "time",
      .datatype = "int32_t",
      .unit = "ms",
      .struct_name = "",
  });
  refSignals.append({.name = "input_bus.mpu6050data.temp",
                     .datatype = "float",
                     .unit = "°C",
                     .struct_name = "MultikopterInput"});
  refSignals.append({.name = "input_bus.mpu6050data.gyx",
                     .datatype = "float",
                     .unit = "°/s",
                     .struct_name = "MultikopterInput"});
  // At least the name, index and the datatype is needed
  refSignals.append({.name = "input_bus.mpu6050data.gyy",
                     .datatype = "float",
                     .unit = "",
                     .struct_name = ""});

  QJsonObject object;

  {
    Signals signals_;
    signals_.setSignals(refSignals);
    signals_.writeToJsonObject(object);
  }

  {
    Signals signals_;
    signals_.parseJsonObject(object);

    QCOMPARE(signals_.getSignalCount(), 4);

    for (int i = 0; i < signals_.getSignalCount(); i++) {
      QCOMPARE(signals_.getSignal(i).index, i);
      QCOMPARE(signals_.getSignal(i).name, refSignals.at(i).name);
      QCOMPARE(signals_.getSignal(i).struct_name, refSignals.at(i).struct_name);
      QCOMPARE(signals_.getSignal(i).unit, refSignals.at(i).unit);
      QCOMPARE(signals_.getSignal(i).datatype, refSignals.at(i).datatype);
      QCOMPARE(signals_.getSignal(i).startByte,
               i * 4); // int32, float, float, float, float
    }
  }
}

void TestSignalSettings::TestSignalImportExport() {
  QString signal_name = "Signalname 123";
  QString datatype = "uint8_t";
  QString unit = "m/s";
  QString struct_name = "Struct";

  QJsonObject obj;
  {
    Signal signal;
    signal.name = signal_name;
    signal.datatype = datatype;
    signal.unit = unit;
    signal.struct_name = struct_name;

    obj = signal.toJsonObject();
  }

  {
    auto signal = Signal::fromJsonObject(obj);
    QCOMPARE(signal.name, signal_name);
    QCOMPARE(signal.datatype, datatype);
    QCOMPARE(signal.unit, unit);
    QCOMPARE(signal.struct_name, struct_name);
  }
}

void TestSignalSettings::TestSignalImportExportInvalidDatatype() {
  QString signal_name = "Signalname 123";
  QString datatype = "invalid datatype";
  QString unit = "m/s";
  QString struct_name = "Struct";

  QJsonObject obj;
  {
    Signal signal;
    signal.name = signal_name;
    signal.datatype = datatype;
    signal.unit = unit;
    signal.struct_name = struct_name;

    obj = signal.toJsonObject();
  }

  {
    auto signal = Signal::fromJsonObject(obj);
    QCOMPARE(signal.name, signal_name);
    QCOMPARE(signal.datatype, "");
    QCOMPARE(signal.unit, unit);
    QCOMPARE(signal.struct_name, struct_name);
  }
}

QTEST_MAIN(TestSignalSettings);
