#include "TestUDP.h"
#include "signals.h"
#include "triggerwidget.h"
#include "udp.h"
#include "plotbuffer.h"

#include "TestUdpSocket.h"

#include <QMutex>
#include <QDateTime>
#include <QUdpSocket>
#include <unistd.h>

void TestUDP::TestRefreshRate() {
  const QHostAddress hostaddress;
  const qint16 port = 7755;
  const int udpBufferSize = 2000;
  const int refreshRate = 50;
  const int useDataCount = 1;
  const QString exportPath = "";
  const QString projectName = "Test";

  QMutex mutex;

  TriggerSettings triggerSettings;
  Signals signals_;

  PlotBuffers buffers(&signals_);

  QThread udp_thread;
  int counter = 0;
  auto* udp = new UDP(&mutex, &buffers, &signals_, triggerSettings);
  udp->init(hostaddress, port, udpBufferSize, refreshRate, useDataCount,
            exportPath, projectName);

  bool stop = false;
  qint64 startTime;
  qint64 stopTime;

  connect(udp, &UDP::dataChanged,
          [this, &counter, &stop, &startTime, &stopTime]() {
            counter++;
            if (counter >= 10) {
              stop = true;
            }

            if (counter == 1) {
              startTime = QDateTime::currentMSecsSinceEpoch();
              stopTime = startTime;
            } else {
              stopTime = QDateTime::currentMSecsSinceEpoch();

              auto difference =
                  abs(stopTime - startTime - 1.0 / refreshRate * 1000);
              // difference must be less than 5ms
              QVERIFY2(difference < 5,
                       qPrintable("Refreshrate not correct. Difference: " +
                                  QString::number(difference)));
              startTime = stopTime;
            }
            emit this->dataChanged(true);
          });
  connect(this, &TestUDP::dataChanged, udp, &UDP::setDataChanged);

  udp->moveToThread(&udp_thread);
  udp_thread.start();

  emit dataChanged(true);

  while (!stop) {
  }

  udp_thread.quit();
  delete udp;
  QCOMPARE(counter, 10);
}

#ifdef SIMULATE_UDP
void TestUDP::TestBindFailing() {
  const QHostAddress hostaddress;
  const qint16 port = 7755;
  const int udpBufferSize = 2000;
  const int refreshRate = 50;
  const int useDataCount = 1;
  const QString exportPath = "";
  const QString projectName = "Test";

  QMutex mutex;

  TriggerSettings triggerSettings;
  Signals signals_;

  PlotBuffers buffers(&signals_);

  QThread udp_thread;
  auto* udp = new UDP(&mutex, &buffers, &signals_, triggerSettings);

  udp->m_socket->setBindOk(false);

  QCOMPARE(udp->init(hostaddress, port, udpBufferSize, refreshRate,
                     useDataCount, exportPath, projectName),
           false);
}

void TestUDP::TestDataRateReceived() {
  QMutex mutex;

  TriggerSettings triggerSettings;
  Signals signals_;

  PlotBuffers buffers(&signals_);

  auto* udp = new UDP(&mutex, &buffers, &signals_, triggerSettings);
  emit udp->m_socket->readyRead();

  // int64_t differences[100];

  const int64_t diff = 10e3;
  usleep(diff);
  for (int i = 0; i < 100; i++) {
    emit udp->m_socket->readyRead();
    const int64_t difference = abs(udp->m_time_difference - diff);
    // differences[i] = difference;
    usleep(diff);
    // maximum 1ms of difference
    QVERIFY2(difference < 1e3,
             qPrintable("Difference: " + QString::number(difference)));
  }
}

void TestUDP::TestGetData() {
  QMutex mutex;

  const QHostAddress hostaddress;
  const qint16 port = 7755;
  const int udpBufferSize = 10;
  const int refreshRate = 50;
  const int useDataCount = 3;
  const QString exportPath = "";
  const QString projectName = "Test";

  TriggerSettings triggerSettings;
  Signals signals_;

  PlotBuffers buffers(&signals_);

  QThread udp_thread;
  auto* udp = new UDP(&mutex, &buffers, &signals_, triggerSettings);
  QCOMPARE(udp->init(hostaddress, port, udpBufferSize, refreshRate,
                     useDataCount, exportPath, projectName),
           true);

  for (int i = 0; i < 100; i++) {
    const auto t = (float)i / 100;
    udp->m_socket->setTime(t);
    udp->m_socket->setTemp(sin(t));
    udp->m_socket->setGyx(cos(t));
    udp->m_socket->setGyy(tan(t));
    udp->m_socket->setGyz(sin(t + M_PI_2));

    emit udp->m_socket->readyRead();

    // Check that udp index stays always a valid index
    // +1 because index was increased here already
    QCOMPARE(udp->m_udp_index, i % udpBufferSize + 1);
  }
}

void TestUDP::TestUseData() {
  // Test if only every third incomming network data is processed
  const QHostAddress hostaddress;
  const qint16 port = 7755;
  const int udpBufferSize = 2000;
  const int refreshRate = 50;
  const int useDataCount = 3;
  const QString exportPath = "";
  const QString projectName = "Test";

  QMutex mutex;

  TriggerSettings triggerSettings;
  QVector<Signal> refSignals;
  refSignals.append(Signal{
      .name = "time",
      .datatype = "int32_t",
  });
  refSignals.append(Signal{
      .name = "data",
      .datatype = "int32_t",
  });
  Signals signals_;
  signals_.setSignals(refSignals);

  PlotBuffers buffers(&signals_);

  auto signal_xaxis = signals_.getSignal(0);
  auto signal_yaxis = signals_.getSignal(1);

  // Generate new buffer. This is the same what plot does
  // to create a new plot
  buffers.getBuffer(signal_xaxis, signal_yaxis);
  QCOMPARE(buffers.m_plot_buffer.size(), 1);

  auto* udp = new UDP(&mutex, &buffers, &signals_, triggerSettings);
  udp->connectDataReady(); // Start filling the buffers
  QCOMPARE(udp->init(hostaddress, port, udpBufferSize, refreshRate,
                     useDataCount, exportPath, projectName),
           true);

  int addedDataToBuffer = 0;
  for (int i = 0; i < 100; i++) {
    emit udp->m_socket->readyRead();

    if (i % useDataCount == 0) {
      addedDataToBuffer++;
    }

    QCOMPARE(udp->m_data_buffers->m_plot_buffer.at(0)->size(),
             addedDataToBuffer);
  }
}

#endif // SIMULATE_UDP

QTEST_MAIN(TestUDP);
