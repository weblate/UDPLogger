#include "TestPlot.h"
#include "plot.h"
#include "plotbuffer.h"

#include <QTemporaryFile>

void TestPlot::TestImportExport() {
  const double ymin = -5;
  const double ymax = +230;
  const bool if_relative_range = true;
  const bool if_automatic_range = false;
  const double automatic_value = 55239;
  const QColor color = QColor(Qt::green);
  const QCPGraph::LineStyle linestyle = QCPGraph::LineStyle::lsImpulse;
  const QCPScatterStyle::ScatterShape scatterstyle =
      QCPScatterStyle::ScatterShape::ssPeace;
  const QString plotName = "Plot1";

  const Signal signal_yaxis{.name = "YaxisSignal"};
  const Signal signal_xaxis{.name = "XaxisSignal"};

  Signals signals_;
  QVector<Signal> s;
  s.append(signal_xaxis);
  s.append(signal_yaxis);
  signals_.setSignals(s);

  QJsonObject obj;
  {
    PlotBuffers plotbuffers(&signals_);
    Plot p(0, &signals_, &plotbuffers, nullptr);

    p.m_settings.ymin = ymin;
    p.m_settings.ymax = ymax;
    p.m_settings.automatic_value = automatic_value;
    p.m_settings.ifrelative_ranging = if_relative_range;
    p.m_settings.ifautomatic_range = if_automatic_range;
    p.m_settings.signal_settings.append({.color = color,
                                         .linestyle = linestyle,
                                         .scatterstyle = scatterstyle,
                                         .signal_yaxis = signal_yaxis,
                                         .signal_xaxis = signal_xaxis,
                                         .name = plotName});

    p.writeJSON(obj);
  }

  {
    PlotBuffers plotbuffers(&signals_);
    Plot p(0, &signals_, &plotbuffers, nullptr);
    p.importSettings(obj);

    QCOMPARE(p.m_settings.ymin, ymin);
    QCOMPARE(p.m_settings.ymax, ymax);
    QCOMPARE(p.m_settings.automatic_value, automatic_value);
    QCOMPARE(p.m_settings.ifrelative_ranging, if_relative_range);
    QCOMPARE(p.m_settings.ifautomatic_range, if_automatic_range);
    QCOMPARE(p.m_settings.signal_settings.count(), 1);
    QCOMPARE(p.m_settings.signal_settings.at(0).name, plotName);
    QCOMPARE(p.m_settings.signal_settings.at(0).color, color);
    QCOMPARE(p.m_settings.signal_settings.at(0).linestyle, linestyle);
    QCOMPARE(p.m_settings.signal_settings.at(0).scatterstyle, scatterstyle);
    QCOMPARE(p.m_settings.signal_settings.at(0).signal_xaxis.name,
             signal_xaxis.name);
    QCOMPARE(p.m_settings.signal_settings.at(0).signal_yaxis.name,
             signal_yaxis.name);
  }
}

void TestPlot::TestPlotBuffersCreatedImport() {
  // Test if the corresponding plot buffers are created

  // Other parameters are checked in the signal test, here only to
  // check if the signal properties being read
  const Signal signal_yaxis{.name = "YaxisSignal"};
  const Signal signal_xaxis{.name = "XaxisSignal"};

  Signals signals_;
  QVector<Signal> s;
  s.append(signal_xaxis);
  s.append(signal_yaxis);
  signals_.setSignals(s);

  QJsonObject obj;
  {
    PlotBuffers plotbuffers(&signals_);
    Plot p(0, &signals_, &plotbuffers, nullptr);

    p.m_settings.signal_settings.append({
        .signal_yaxis = signal_yaxis,
        .signal_xaxis = signal_xaxis,
    });

    p.writeJSON(obj);
  }

  {
    PlotBuffers plotbuffers(&signals_);
    Plot p(0, &signals_, &plotbuffers, nullptr);
    p.importSettings(obj);

    QCOMPARE(p.m_settings.signal_settings.at(0).signal_xaxis.name,
             signal_xaxis.name);
    QCOMPARE(p.m_settings.signal_settings.at(0).signal_yaxis.name,
             signal_yaxis.name);
  }
}

QTEST_MAIN(TestPlot);
