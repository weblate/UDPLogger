#ifndef TESTPLOT_H
#define TESTPLOT_H

#include "CommonTest.h"

class TestPlot : public CommonTest {
  Q_OBJECT

private slots:
  void TestImportExport();
  void TestPlotBuffersCreatedImport();
};

#endif // TESTPLOT_H
