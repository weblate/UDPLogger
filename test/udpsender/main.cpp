#include <QUdpSocket>
#include <QDateTime>
#include <QtMath>

#include "udpPacket.h"

int main(int argc, char* argv[]) {
  QUdpSocket socket;

  socket.connectToHost(QHostAddress::LocalHost, 7755, QIODevice::WriteOnly);

  double sendRate = 1 / 1000;

  qint64 programStartTime = QDateTime::currentMSecsSinceEpoch();
  qint64 startTime = programStartTime;
  qint64 time;

  char buffer[20]; // 4 bytes for every variable (time, temp, gyx, gyy, gyz)
  float temp;
  float gyx;
  float gyy;
  float gyz;

  while (1) {
    time = QDateTime::currentMSecsSinceEpoch();

    if ((double)(time - startTime) > sendRate) {
      startTime = time;

      auto factor = (double)time / 1000 * 2 * M_PI * 10;
      temp = qSin(factor);
      gyx = qCos(factor);
      gyy = qCos(factor + M_PI_2);
      gyz = qCos(factor + M_PI_4);

      if (createUDPPackage(buffer, sizeof(buffer),
                           (int32_t)(time - programStartTime), temp, gyx, gyy,
                           gyz))
        break; // Something went wrong

      socket.write(buffer, sizeof(buffer));
    }
  }
}
