#ifndef CREATEUDPPACKAGE_H
#define CREATEUDPPACKAGE_H
#include <stdint.h>
int createUDPPackage(char* udp_buffer, const int buffer_length,
                     const int32_t time, const float temp, const float gyx,
                     const float gyy, const float gyz);
#endif // CREATEUDPPACKAGE_H