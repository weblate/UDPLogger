add_executable(TestMainWindow TestMainWindow.cpp)
target_link_libraries(TestMainWindow CommonTest UDPLoggerLib)

add_test(NAME TestMainWindow COMMAND TestMainWindow)
