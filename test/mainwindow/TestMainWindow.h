#ifndef TESTMAINWINDOW_H
#define TESTMAINWINDOW_H

#include "CommonTest.h"

class TestMainWindow : public CommonTest {
  Q_OBJECT

private slots:
  void TestImportExport();
};

#endif // TESTMAINWINDOW_H
