add_executable(TestPlots TestPlots.cpp)
target_link_libraries(TestPlots CommonTest UDPLoggerLib)

add_test(NAME TestPlots COMMAND TestPlots)
