#include "TestPlots.h"
#include "plots.h"
#include "plot.h"
#include "triggerwidget.h"
#include "udp.h"
#include "settingsdialog.h"

#include <QTemporaryFile>

const QString project_name = "Test123";
const int port = 123;
const QHostAddress hostaddress("129.168.3.4");
const int udp_buffer_size = 573;
const int user_data_count = 2390;
const int plot_buffer_size = 3409283;

const int refresh_rate = 1239;
const QString data_export_path = "/home/$USER/test123";

void TestPlots::TestImportExport() {

  QJsonObject obj;
  {
    Signals signals_;
    TriggerWidget tw(&signals_, nullptr);

    Plots plots(nullptr, &signals_, &tw);

    // Change values from default
    plots.m_project_name = project_name;

    plots.m_hostaddress = hostaddress;
    plots.m_port = port;
    plots.m_udp_buffer_size = udp_buffer_size;
    plots.m_use_data_count = user_data_count;

    plots.m_refresh_rate = refresh_rate;
    // PlotBuffersize
    plots.m_data_buffers->setPlotBufferSize(plot_buffer_size);
    plots.m_data_export_path = data_export_path;
    obj = plots.toJsonObject();
  }

  {
    Signals signals2;
    TriggerWidget tw(&signals2, nullptr);
    Plots p2(nullptr, &signals2, &tw);
    QCOMPARE(p2.fromJsonObject(obj), 0);

    QCOMPARE(p2.m_project_name, project_name);

    QCOMPARE(p2.m_hostaddress, hostaddress);
    QCOMPARE(p2.m_port, port);
    QCOMPARE(p2.m_udp_buffer_size, udp_buffer_size);
    QCOMPARE(p2.m_use_data_count, user_data_count);

    QCOMPARE(p2.m_refresh_rate, refresh_rate);
    QCOMPARE(p2.m_data_buffers->bufferSize(), plot_buffer_size);
    QCOMPARE(p2.m_data_export_path, data_export_path);
  }
}

void TestPlots::TestImportExportSignals() {
  // Test if signals are applied correctly to the plots

  QVector<Signal> s{{
                        .name = "Var1",
                        .datatype = "uint8_t",
                        .unit = "m/s",
                    },
                    {
                        .name = "Var2",
                        .datatype = "double",
                        .unit = "m/s",
                    }};
  QJsonObject obj;
  {
    Signals signals_;
    signals_.setSignals(s);
    TriggerWidget tw(&signals_, nullptr);

    Plots plots(nullptr, &signals_, &tw);
    plots.createNewPlot();
    QCOMPARE(plots.m_plots.count(), 1);

    // Add one curve to the plot
    SignalSettings sigSettings;
    sigSettings.signal_xaxis = s.at(0);
    sigSettings.signal_yaxis = s.at(1);
    PlotSettings settings;
    settings.signal_settings.append(sigSettings);
    plots.m_plots.at(0)->updateSettings(settings);

    obj = plots.toJsonObject();
  }

  {
    Signals signals2;
    TriggerWidget tw(&signals2, nullptr);
    Plots p2(nullptr, &signals2, &tw);
    QCOMPARE(p2.fromJsonObject(obj), 0);

    QCOMPARE(signals2.getSignalCount(), 2);

    QCOMPARE(signals2.getSignal(0).index, 0);
    QCOMPARE(signals2.getSignal(0).name, "Var1");
    QCOMPARE(signals2.getSignal(0).struct_name, "");
    QCOMPARE(signals2.getSignal(0).unit, "m/s");
    QCOMPARE(signals2.getSignal(0).datatype, "uint8_t");
    QCOMPARE(signals2.getSignal(0).startByte, 0);

    QCOMPARE(signals2.getSignal(1).index, 1);
    QCOMPARE(signals2.getSignal(1).name, "Var2");
    QCOMPARE(signals2.getSignal(1).struct_name, "");
    QCOMPARE(signals2.getSignal(1).unit, "m/s");
    QCOMPARE(signals2.getSignal(1).datatype, "double");
    QCOMPARE(signals2.getSignal(1).startByte, 1);

    QCOMPARE(p2.m_plots.count(), 1);
    auto* plot = p2.m_plots.at(0);

    auto signal_settings = plot->m_settings.signal_settings;
    QCOMPARE(signal_settings.count(), 1);
    QCOMPARE(signal_settings.at(0).signal_xaxis.name, s.at(0).name);
    QCOMPARE(signal_settings.at(0).signal_xaxis.datatype, s.at(0).datatype);
    QCOMPARE(signal_settings.at(0).signal_xaxis.unit, s.at(0).unit);
    QCOMPARE(signal_settings.at(0).signal_xaxis.index, 0);
    QCOMPARE(signal_settings.at(0).signal_xaxis.startByte, 0);
    QCOMPARE(signal_settings.at(0).signal_yaxis.name, s.at(1).name);
    QCOMPARE(signal_settings.at(0).signal_yaxis.datatype, s.at(1).datatype);
    QCOMPARE(signal_settings.at(0).signal_yaxis.unit, s.at(1).unit);
    QCOMPARE(signal_settings.at(0).signal_yaxis.index, 1);
    QCOMPARE(signal_settings.at(0).signal_yaxis.startByte, 1);
  }
}

void TestPlots::TestInitUDP() {
  Signals signals_;
  TriggerWidget tw(&signals_, nullptr);
  QJsonObject obj;

  {
    Plots plots(nullptr, &signals_, &tw);
    // Change values from default
    plots.m_project_name = project_name;

    plots.m_hostaddress = hostaddress;
    plots.m_port = port;
    plots.m_udp_buffer_size = udp_buffer_size;
    plots.m_use_data_count = user_data_count;
    plots.m_refresh_rate = refresh_rate;
    plots.m_data_export_path = data_export_path;
    obj = plots.toJsonObject();
  }

  {
    int initUDPCalledCounter = 0;
    Signals signals2;
    Plots p2(nullptr, &signals2, &tw);
    connect(
        &p2, &Plots::initUDP,
        [&initUDPCalledCounter](const QHostAddress& ha, const quint16 p,
                                const int udpbs, const int rr, const int udc,
                                const QString& ep, const QString& pn) {
          QCOMPARE(ha, hostaddress);
          QCOMPARE(p, port);
          QCOMPARE(udpbs, udp_buffer_size);
          QCOMPARE(rr, refresh_rate);
          QCOMPARE(udc, user_data_count);
          QCOMPARE(ep, SettingsDialog::createExportPath("", data_export_path));
          QCOMPARE(pn, project_name);
          initUDPCalledCounter++;
        });
    QCOMPARE(p2.fromJsonObject(obj), 0);
    QCOMPARE(initUDPCalledCounter, 1);

    QCOMPARE(p2.m_udp->m_ifread_data, false);
  }
}

QTEST_MAIN(TestPlots);
