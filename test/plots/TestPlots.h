#ifndef TESTPLOTS_H
#define TESTPLOTS_H

#include "CommonTest.h"

class TestPlots : public CommonTest {
  Q_OBJECT

private slots:
  void TestImportExport();
  void TestImportExportSignals();
  void TestInitUDP();
};

#endif // TESTPLOTS_H
