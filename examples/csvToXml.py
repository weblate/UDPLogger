import sys
import os
import csv

def createSignal(index: str, name: str, unit: str, structname: str, datatype: str):
    return f'<Signal index="{index}" name="{name}" structname="{structname}" unit="{unit}" datatype="{datatype}">'

if __name__ == '__main__':
    if len(sys.argv) < 2:
        raise Exception("Pass csv as argument to this script")

    file = sys.argv[1]
    if not os.path.isfile(file):
        raise Exception("File does not exist")

    xmlstr = ""

    with open(file, newline="") as csvfile:
        reader = csv.reader(csvfile, delimiter=",")
        for row in reader:
            row_split = row
            xmlstr += createSignal(row[0], row[1], row[2], row[3], row[4]) + "\n"
    xmlstr = f"<Signals>\n{xmlstr}</Signals>"

    with open(file + ".xml", "w") as f:
        f.write(xmlstr)

