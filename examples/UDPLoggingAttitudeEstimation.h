#ifndef CREATEUDPPACKAGE_H
#define CREATEUDPPACKAGE_H
#include <stdint.h> 
int createUDPPackage(char*  udp_buffer, const int buffer_length, const int32_t time, const struct MultikopterInput* input_bus, const struct MultikopterOutput* output_bus, const uint64_t program_execution_time, const struct matlab_signals_T* multikopter_signals, const uint64_t measurementLoopStep, const uint64_t mainLoopStep, const uint64_t measurementLoopExecutionTime, const int32_t ReglerKP, const int32_t ReglerKI);
#endif //CREATEUDPPACKAGE_H