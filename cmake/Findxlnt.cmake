# Findxlnt.cmake
#
# Finds the xlnt library
#
# This will define the following variables
#
#   xlnt_FOUND
#   xlnt_LIBRARY
#   xlnt_LIBRARIES
#   xlnt_LIBRARY_DEBUG
#   xlnt_LIBRARY_RELEASE
#
# and the following imported targets
#
#   xlnt::xlnt
#
# Author: John Coffey - johnco3@gmail.com
#

find_path(xlnt_INCLUDE_DIR NAMES xlnt/xlnt.hpp)

if (NOT xlnt_LIBRARIES)
    find_library(xlnt_LIBRARY_RELEASE NAMES xlnt DOC "xlnt release library")
    find_library(xlnt_LIBRARY_DEBUG NAMES xlntd DOC "xlnt debug library")
    include(SelectLibraryConfigurations)
    select_library_configurations(xlnt)
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(xlnt
    REQUIRED_VARS xlnt_INCLUDE_DIR xlnt_LIBRARY)
mark_as_advanced(
    xlnt_INCLUDE_DIR
    xlnt_LIBRARY)

if(xlnt_FOUND AND NOT (TARGET xlnt::xlnt))
    # debug output showing the located libraries
    message(STATUS "xlnt_INCLUDE_DIR=${xlnt_INCLUDE_DIR}")
    message(STATUS "xlnt_LIBRARY=${xlnt_LIBRARY}")
    message(STATUS "xlnt_LIBRARIES=${xlnt_LIBRARIES}")
    message(STATUS "xlnt_LIBRARY_DEBUG=${xlnt_LIBRARY_DEBUG}")
    message(STATUS "xlnt_LIBRARY_RELEASE=${xlnt_LIBRARY_RELEASE}")
    # Add a blank imported library
    add_library(xlnt::xlnt UNKNOWN IMPORTED)

    # add the transitive includes property
    set_target_properties(xlnt::xlnt PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${xlnt_INCLUDE_DIR}")

    # Optimized library
    if(xlnt_LIBRARY_RELEASE)
        set_property(TARGET xlnt::xlnt APPEND PROPERTY
            IMPORTED_CONFIGURATIONS RELEASE)
        set_target_properties(xlnt::xlnt PROPERTIES
            IMPORTED_LOCATION_RELEASE "${xlnt_LIBRARY_RELEASE}")
    endif()

    # Debug library
    if(xlnt_LIBRARY_DEBUG)
        set_property(TARGET xlnt::xlnt APPEND PROPERTY
            IMPORTED_CONFIGURATIONS DEBUG)
        set_target_properties(xlnt::xlnt PROPERTIES
            IMPORTED_LOCATION_DEBUG "${xlnt_LIBRARY_DEBUG}")
    endif()

    # some other configuration
    if(NOT xlnt_LIBRARY_RELEASE AND NOT xlnt_LIBRARY_DEBUG)
        set_property(TARGET xlnt::xlnt APPEND PROPERTY
            IMPORTED_LOCATION "${xlnt_LIBRARY}")
    endif()
endif() 
