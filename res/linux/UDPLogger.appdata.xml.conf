<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop-application">
  <id>@UDPLogger_ID@</id>
  <metadata_license>FSFAP</metadata_license>
  <project_license>GPL-3.0</project_license>
  <name>UDPLogger</name>
  <summary>@UDPLogger_Description@</summary>

  <description>
    <p>Features:</p>
    <ul>
      <li>Saving UDP data in netcdf files</li>
	  <li>Plotting UDP data in realtime</li>
	  <li>Importing signal names from *.xlsx, *.xml</li>
    </ul>
    
    Workflow:
    <ul>
      <li> Define signals in a xml or excel file </li>
      <li> Import signals into the UDPLogger </li>
      <li> Configure UDP </li>
      <li> Save settings to file to reuse it </li>
      <li> Start UDP logging </li>
    </ul>
  </description>

  <launchable type="desktop-id">@UDPLogger_ID@.desktop</launchable>

  <screenshots>
    <screenshot type="default">
      <caption>The main view</caption>
      <image>https://gitlab.com/Murmele/UDPLogger/-/raw/master/res/images/MainView.png</image>
    </screenshot>
    <screenshot>
      <caption>Modifying curves</caption>
      <image>https://gitlab.com/Murmele/UDPLogger/-/raw/master/res/images/CurveSettings.png</image>
    </screenshot>
  </screenshots>

  <url type="homepage">https://murmele.gitlab.io/UDPLogger/</url>

  <provides>
    <binary>UDPLogger</binary>
    <id>UDPLogger</id>
  </provides>

  <releases>
    <release version="2.6.1" date="2022-09-07">
      <description>
        <p>Fix Excel import</p>
      </description>
    </release>
    <release version="2.6.0" date="2022-09-02">
      <description>
        <p>Refactoring and Unittests</p>
      </description>
    </release>
    <release version="1.0" date="2018-09-14">
      <description>
        <p>Initial Flathub release</p>
      </description>
    </release>
  </releases>

 <content_rating type="oars-1.1">
    <content_attribute id="violence-cartoon">none</content_attribute>
    <content_attribute id="violence-fantasy">none</content_attribute>
    <content_attribute id="violence-realistic">none</content_attribute>
    <content_attribute id="violence-bloodshed">none</content_attribute>
    <content_attribute id="violence-sexual">none</content_attribute>
    <content_attribute id="violence-desecration">none</content_attribute>
    <content_attribute id="violence-slavery">none</content_attribute>
    <content_attribute id="violence-worship">none</content_attribute>
    <content_attribute id="drugs-alcohol">none</content_attribute>
    <content_attribute id="drugs-narcotics">none</content_attribute>
    <content_attribute id="drugs-tobacco">none</content_attribute>
    <content_attribute id="sex-nudity">none</content_attribute>
    <content_attribute id="sex-themes">none</content_attribute>
    <content_attribute id="sex-homosexuality">none</content_attribute>
    <content_attribute id="sex-prostitution">none</content_attribute>
    <content_attribute id="sex-adultery">none</content_attribute>
    <content_attribute id="sex-appearance">none</content_attribute>
    <content_attribute id="language-profanity">none</content_attribute>
    <content_attribute id="language-humor">none</content_attribute>
    <content_attribute id="language-discrimination">none</content_attribute>
    <content_attribute id="social-chat">none</content_attribute>
    <content_attribute id="social-info">none</content_attribute>
    <content_attribute id="social-audio">none</content_attribute>
    <content_attribute id="social-location">none</content_attribute>
    <content_attribute id="social-contacts">none</content_attribute>
    <content_attribute id="money-purchasing">none</content_attribute>
    <content_attribute id="money-gambling">none</content_attribute>
  </content_rating>
  
</component>

