## Testing
In `test/udpsender` the source for a simple udp sender can be found.

1) Build the sender and start it.

2) Open UDPLogger and import the file located in `test/udpsender`.

3) Start UDP logging

## Flatpak
- flatpak-builder --repo=UDPLoggerRepo --force-clean UDPLogger com.github.Murmele.UDPLogger.json
- flatpak remote-add UDPLoggerRepo UDPLoggerRepo --no-gpg-verify
- flatpak install UDPLoggerRepo com.github.Murmele.UDPLogger

## Build Instructions
- xlnt: https://github.com/tfussell/xlnt
- netcdf_c++14: https://www.unidata.ucar.edu/downloads/netcdf/index.jsp
	- Download source. Set Debug=true in configure
	- ./configure
	- make
	- sudo make install

For debugging, the libraries must be installed with debug information
