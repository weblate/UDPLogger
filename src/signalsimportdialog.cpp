#include "signalsimportdialog.h"
#include "ui_signalsimportdialog.h"
#include "signals.h"

SignalsImportDialog::SignalsImportDialog(Signals* s, QWidget* parent)
    : QDialog(parent), ui(new Ui::SignalsImportDialog) {
  ui->setupUi(this);
  ui->lw_errors->hide();
  ui->te_error->hide();
  ui->l_errors->setText(tr("Successfully imported"));

  ui->l_errors->setText(tr("Importing..."));
  connect(ui->btn_ok, &QPushButton::clicked, this, &QDialog::accept);
  connect(s, &Signals::invalidSignal, this, &SignalsImportDialog::addMessage);
  connect(s, &Signals::importFailed, this, &SignalsImportDialog::importFailed);

  s->importSignals();
}

SignalsImportDialog::~SignalsImportDialog() { delete ui; }

void SignalsImportDialog::importFailed(const QString& title,
                                       const QString& description) {
  ui->l_errors->setText(title);
  ui->te_error->setText(description);
  ui->te_error->show();
}

void SignalsImportDialog::addMessage(const QString& message) {
  ui->l_errors->setText(tr("Errors:"));
  ui->lw_errors->show();
  ui->lw_errors->addItem(message);
}
