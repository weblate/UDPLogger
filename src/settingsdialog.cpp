/***
 *  This file is part of UDPLogger
 *
 *  Copyright (C) 2018 Martin Marmsoler, martin.marmsoler at gmail.com
 *
 *  UDPLogger is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with UDPLogger.  If not, see <http://www.gnu.org/licenses/>.
 ***/

#include "settingsdialog.h"
#include "ui_settingsdialog.h"
#include "plots.h"

SettingsDialog::SettingsDialog(const QString& settings_filename, Plots* parent)
    : QDialog(parent), ui(new Ui::SettingsDialog),
      m_settings_filename(settings_filename) {
  ui->setupUi(this);
  ui->spinbox_plot_buffer->setRange(1, 2147483647);
  ui->spinbox_udp_buffer->setRange(1, 2147483647);
  ui->spinbox_port->setRange(0, 65535);
  ui->spinbox_refresh_rate->setRange(1, 300);
  ui->txt_hostaddress->setInputMask("000.000.000.000;_");
  ui->spinbox_use_element_count->setRange(1, 2147483647);

  ui->combo_hostname->addItem(tr("address"), QHostAddress::Null);
  ui->combo_hostname->addItem(tr("AnyIPv4"), QHostAddress::AnyIPv4);
  ui->combo_hostname->addItem(tr("LocalHost"), QHostAddress::LocalHost);
  ui->combo_hostname->addItem(tr("Broadcast"), QHostAddress::Broadcast);
  ui->txt_relative_header_path->setText("");

  connect(ui->buttonBox, &QDialogButtonBox::accepted, this,
          &SettingsDialog::accepted);
  connect(ui->combo_hostname, qOverload<int>(&QComboBox::currentIndexChanged),
          this, &SettingsDialog::comboHostnameIndexChanged);
  connect(ui->leExportSourceFilePath, &QLineEdit::textChanged, this,
          &SettingsDialog::leSourcePathTextChanged);
  connect(ui->txt_relative_header_path, &QLineEdit::textChanged, this,
          &SettingsDialog::leHeaderPathTextChanged);
}

void SettingsDialog::setSettings(const Settings& settings) {
  ui->txt_project_name->setText(settings.projectName);
  ui->txt_export_path->setText(settings.exportFilePath);

  ui->spinbox_plot_buffer->setValue(settings.plotBufferSize);
  ui->spinbox_refresh_rate->setValue(settings.refreshRate);

  ui->combo_hostname->setCurrentIndex(0); // TODO: why?
  ui->txt_hostaddress->setText(settings.hostaddress.toString());
  ui->spinbox_port->setValue(settings.port);
  ui->spinbox_udp_buffer->setValue(settings.udpBufferSize);
  ui->spinbox_use_element_count->setValue(settings.useDataCount);

  ui->leExportSourceFilePath->setText(settings.sourcePath);
  ui->txt_relative_header_path->setText(settings.relativeHeaderPath);
  ui->txt_additional_includes->setText(settings.additionalIncludes);
}

void SettingsDialog::settings(Settings& settings) const {
  settings.projectName = ui->txt_project_name->text();
  settings.exportFilePath = ui->txt_export_path->text();

  settings.plotBufferSize = static_cast<int>(ui->spinbox_plot_buffer->value());
  settings.refreshRate = static_cast<int>(ui->spinbox_refresh_rate->value());

  settings.hostaddress = QHostAddress(ui->txt_hostaddress->text());
  settings.port = static_cast<int>(ui->spinbox_port->value());
  settings.udpBufferSize = static_cast<int>(ui->spinbox_udp_buffer->value());
  settings.useDataCount =
      static_cast<int>(ui->spinbox_use_element_count->value());

  settings.sourcePath = ui->leExportSourceFilePath->text();
  settings.relativeHeaderPath = ui->txt_relative_header_path->text();
  settings.additionalIncludes = ui->txt_additional_includes->text();
}

void SettingsDialog::comboHostnameIndexChanged(int index) {
  int address = ui->combo_hostname->itemData(index).toInt();

  if (address == QHostAddress::AnyIPv4) {
    ui->txt_hostaddress->setText("0.0.0.0");
  } else if (address == QHostAddress::Broadcast) {
    ui->txt_hostaddress->setText("255.255.255.255");
  } else if (address == QHostAddress::LocalHost) {
    ui->txt_hostaddress->setText("127.0.0.1");
  }
}

SettingsDialog::~SettingsDialog() { delete ui; }

void SettingsDialog::on_btn_browse_export_file_clicked() {
  QString path = QFileDialog::getExistingDirectory(
      this, tr("Set export file path"), "/home");
  ui->txt_export_path->setText(path);
}

void SettingsDialog::on_pbBrowseSourcePath_clicked() {
  QString path = QFileDialog::getExistingDirectory(
      this, tr("Set export file path"), "/home");
  ui->leExportSourceFilePath->setText(path);
}

void SettingsDialog::on_txt_export_path_textChanged(const QString& text) {
  Q_UNUSED(text)
  checkSettingsOK();
}

void SettingsDialog::leSourcePathTextChanged(const QString& text) {
  Q_UNUSED(text)
  checkSettingsOK();
}

void SettingsDialog::leHeaderPathTextChanged(const QString& text) {
  Q_UNUSED(text)
  checkSettingsOK();
}

QString SettingsDialog::createExportPath(const QString& signal_settings_file,
                                         const QString& path) {
  const QDir export_path(path);

  if (export_path.isRelative()) {
    const QFileInfo settings_file(signal_settings_file);
    QDir settings_path(settings_file.absolutePath());
    if (settings_path.cd(path))
      return settings_path.absolutePath();
  } else {
    if (export_path.exists())
      return path;
  }
  return "";
}

bool SettingsDialog::pathExists(const QString& path) const {
  const QFileInfo info(createExportPath(m_settings_filename, path));
  return info.exists();
}

void SettingsDialog::checkSettingsOK() {
  if (!pathExists(ui->txt_export_path->text())) {
    ui->buttonBox->button(QDialogButtonBox::StandardButton::Ok)
        ->setEnabled(false);
    ui->buttonBox->setToolTip(tr("Data export path does not exist"));
    ui->txt_export_path->setStyleSheet("QLineEdit{background:red;}");
  } else {
    ui->buttonBox->button(QDialogButtonBox::StandardButton::Ok)
        ->setEnabled(true);
    ui->txt_export_path->setStyleSheet(QString());
  }

  if (!QDir(ui->leExportSourceFilePath->text()).exists()) {
    ui->buttonBox->button(QDialogButtonBox::StandardButton::Ok)
        ->setEnabled(false);
    ui->buttonBox->setToolTip(tr("ExportSourceFile path is not valid"));
    ui->leExportSourceFilePath->setStyleSheet("QLineEdit{background:red;}");
    return;
  } else {
    ui->leExportSourceFilePath->setStyleSheet(QString());
  }

  if (!QDir(ui->leExportSourceFilePath->text() + "/" +
            ui->txt_relative_header_path->text())
           .exists()) {
    ui->buttonBox->button(QDialogButtonBox::StandardButton::Ok)
        ->setEnabled(false);
    ui->buttonBox->setToolTip(tr("Relative header path does not exist"));
    ui->txt_relative_header_path->setStyleSheet("QLineEdit{background:red;}");
    return;
  } else {
    ui->txt_relative_header_path->setStyleSheet(QString());
  }
  ui->buttonBox->setToolTip(tr("Acccept settings"));
  ui->buttonBox->button(QDialogButtonBox::StandardButton::Ok)->setEnabled(true);
}
