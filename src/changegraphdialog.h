/***
 *  This file is part of UDPLogger
 *
 *  Copyright (C) 2018 Martin Marmsoler, martin.marmsoler at gmail.com
 *
 *  UDPLogger is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with UDPLogger.  If not, see <http://www.gnu.org/licenses/>.
 ***/

#ifndef CHANGEGRAPHDIALOG_H
#define CHANGEGRAPHDIALOG_H

#include <QDialog>
#include "signals.h"
#include "plot.h"

class Plot;

namespace Ui {
class changeGraphDialog;
}

class ChangeGraphDialog : public QDialog {
  Q_OBJECT

public:
  explicit ChangeGraphDialog(const Signals* signal,
                             const PlotSettings& settings, QWidget* parent);

  // void saveOldSettings();
  PlotSettings plotSettings() { return m_settings; }
  int getSignalCount() { return m_settings.signal_settings.count(); }
  /*!
   * \brief updateSignals
   * Update signal comboboxes
   */
  void updateSignals();
  void setSettings(const struct PlotSettings& settings);
  void disableSignalSettings(bool disable);

  ~ChangeGraphDialog();
signals:
  void settingsChanged(const PlotSettings&);
public slots:
  void addElement(const SignalSettings* settings_import);
private slots:
  void apply();
  void ok();
  void cancel();
  void deleteElement();
  void addElement();
  void updateData(const QString& text);
  void updateData2();
  void listWidgetRowChanged(int row);
  void on_spinbox_y_min_valueChanged(double arg1);
  void on_spinbox_range_adjustment_valueChanged(double arg1);
  void on_spinbox_y_max_valueChanged(double arg1);
  void on_checkbox_if_automatic_range_toggled(bool checked);
  void on_rb_relative_toggled(bool checked);

private:
  Ui::changeGraphDialog* ui;
  PlotSettings m_settings;
  int m_previous_row{-1};

  const Signals* m_signals;
};

#endif // CHANGEGRAPHDIALOG_H
