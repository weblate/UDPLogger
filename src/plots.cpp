/***
 *  This file is part of UDPLogger
 *
 *  Copyright (C) 2018 Martin Marmsoler, martin.marmsoler at gmail.com
 *
 *  UDPLogger is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with UDPLogger.  If not, see <http://www.gnu.org/licenses/>.
 ***/

#include "plots.h"
#include <QDebug>
#include "udp.h"
#include "signals.h"
#include <QFileDialog>
#include <QMessageBox>
#include "settingsdialog.h"
#include "plot.h"
#include "triggerwidget.h"

namespace JsonSections {
const QString plots = "Plots";
const QString settings = "Settings";
const QString project_name = "ProjectName";
const QString program_version = "ProgramVersion";
const QString plot_buffer_size = "PlotBufferSize";
const QString refresh_rate = "RefreshRate";
const QString skip_element = "SkipElement";
const QString export_data_path = "ExportDataPath";
const QString network_settings = "NetworkSettings";
const QString host_address = "HostAddress";
const QString port = "Port";
const QString UDP_buffer_size = "UDPBufferSize";
} // namespace JsonSections

Plots::Plots(QWidget* parent, Signals* signal, TriggerWidget* trigger)
    : m_parent(parent), m_signals(signal) {
  m_layout = new QVBoxLayout(this);
  m_layout->setSizeConstraint(QLayout::SetMinimumSize);

  m_data_buffers = new PlotBuffers(signal);

  m_mutex = new QMutex;
  m_udp = new UDP(m_mutex, m_data_buffers, signal, trigger->settings());
  connect(m_udp, &UDP::showInfoMessageBox, this, &Plots::showInfoMessageBox,
          Qt::ConnectionType::QueuedConnection);
  connect(m_udp, &UDP::triggerFinished, trigger, &TriggerWidget::triggered,
          Qt::ConnectionType::QueuedConnection);
  connect(m_udp, &UDP::disableTrigger, trigger, &TriggerWidget::disableTrigger,
          Qt::ConnectionType::BlockingQueuedConnection);
  connect(m_udp, &UDP::newTriggerValue, trigger,
          &TriggerWidget::newTriggerValue,
          Qt::ConnectionType::QueuedConnection);
  connect(trigger, &TriggerWidget::startTrigger, m_udp, &UDP::startTrigger,
          Qt::ConnectionType::BlockingQueuedConnection);
  connect(trigger, &TriggerWidget::settingsChanged, m_udp,
          &UDP::setTriggerSettings,
          Qt::ConnectionType::BlockingQueuedConnection);
  connect(m_udp, &UDP::triggerStarted, trigger, &TriggerWidget::triggerStarted,
          Qt::ConnectionType::QueuedConnection);
  connect(m_udp, &UDP::triggerFinished, trigger,
          &TriggerWidget::triggerFinished,
          Qt::ConnectionType::QueuedConnection);
  connect(this, &Plots::connectToReadyRead, m_udp, &UDP::connectDataReady,
          Qt::ConnectionType::QueuedConnection);
  connect(this, &Plots::disconnectToReadyRead, m_udp, &UDP::disconnectDataReady,
          Qt::ConnectionType::QueuedConnection);
  qRegisterMetaType<QHostAddress>(
      "QHostAddress"); // register QHostAddress to be usable in signal/slots
  connect(this, &Plots::initUDP, m_udp,
          qOverload<const QHostAddress&, quint16, int, int, int, const QString&,
                    const QString&>(&UDP::init),
          Qt::ConnectionType::QueuedConnection);

  connect(this, &Plots::plotBufferSizeChanged, m_data_buffers,
          &PlotBuffers::setPlotBufferSize);

  m_udp->moveToThread(&m_udp_thread);
  m_udp_thread.start();
}

QSharedPointer<QCPGraphDataContainer> Plots::getBuffer(const Signal& xaxis,
                                                       const Signal& yaxis) {

  QSharedPointer<QCPGraphDataContainer> pointer =
      m_data_buffers->getBuffer(xaxis, yaxis);
  return pointer;
}

void Plots::removeGraph(const Signal& xaxis, const Signal& yaxis) {
  m_data_buffers->removeGraph(xaxis, yaxis);
}

void Plots::deletePlot(Plot* plot_address) {

  plot_address->deleteLater();

  for (int i = 0; i < m_plots.length(); i++) {
    if (m_plots.at(i) == plot_address) {
      m_plots.remove(i);
      break;
    }
  }
}

QJsonObject Plots::toJsonObject() {
  QJsonObject object;

  object[JsonSections::project_name] = m_project_name;
  object[JsonSections::program_version] = UDPLogger_VERSION;

  QJsonArray plots;
  for (int i = 0; i < m_plots.length(); i++) {
    QJsonObject plotObject;
    m_plots[i]->writeJSON(plotObject);
    plots.append(plotObject);
  }
  object[JsonSections::plots] = plots;

  m_signals->writeToJsonObject(object);

  QJsonObject settings;
  QJsonObject network_settings;
  network_settings[JsonSections::host_address] = m_hostaddress.toString();
  network_settings[JsonSections::port] = m_port;
  network_settings[JsonSections::UDP_buffer_size] = m_udp_buffer_size;
  settings[JsonSections::network_settings] = network_settings;
  settings[JsonSections::plot_buffer_size] = m_data_buffers->bufferSize();
  settings[JsonSections::export_data_path] = m_data_export_path;
  settings[JsonSections::refresh_rate] = m_refresh_rate;
  settings[JsonSections::skip_element] = m_use_data_count;
  object[JsonSections::settings] = settings;
  return object;
}

int Plots::fromJsonObject(const QJsonObject& object) {
  // before importing remove everything
  int size = m_plots.size();
  for (int i = 0; i < size; i++) {
    deletePlot(m_plots.at(0));
  }

  // signals
  m_signals->parseJsonObject(object);

  // plot settings
  if (object.contains(JsonSections::plots)) {
    QJsonArray plots = object[JsonSections::plots].toArray();

    for (int i = 0; i < plots.count(); i++) {
      Plot* plot = createNewPlot();

      QJsonObject plot_settings = plots[i].toObject();
      plot->importSettings(plot_settings);
    }
  }

  m_project_name = object[JsonSections::project_name].toString();

  // object[JsonSections::program_version].toString();

  // global settings
  if (object.contains(JsonSections::settings)) {
    auto settings = object[JsonSections::settings].toObject();
    m_data_buffers->setPlotBufferSize(
        settings[JsonSections::plot_buffer_size].toInt());
    m_refresh_rate = settings[JsonSections::refresh_rate].toInt();
    m_use_data_count = settings[JsonSections::skip_element].toInt();
    m_data_export_path = settings[JsonSections::export_data_path].toString();
    QJsonObject network_settings =
        settings[JsonSections::network_settings].toObject();
    m_hostaddress =
        QHostAddress(network_settings[JsonSections::host_address].toString());
    m_port = network_settings[JsonSections::port].toInt();
    m_udp_buffer_size = network_settings[JsonSections::UDP_buffer_size].toInt();
  }

  emit initUDP(
      m_hostaddress, static_cast<quint16>(m_port), m_udp_buffer_size,
      m_refresh_rate, m_use_data_count,
      SettingsDialog::createExportPath(m_settings_filename, m_data_export_path),
      m_project_name);
  return 0;
}

Plot* Plots::createNewPlot() {
  Plot* plot = new Plot(m_plots.length(), m_signals, m_data_buffers, m_parent);
  connect(plot, &Plot::removeSignal, m_data_buffers,
          &PlotBuffers::removeSignal);
  connect(plot, &Plot::graphRemoved, this, &Plots::removeGraph);
  connect(plot, &Plot::deletePlot2, this, &Plots::deletePlot);
  m_plots.append(plot);
  m_layout->addWidget(plot);

  connect(m_udp, &UDP::dataChanged, plot, &Plot::newData);
  return plot;
}

void Plots::startUDP() {
  emit connectToReadyRead();
  m_ifudpLogging = 1;
}

void Plots::stopUDP() {
  emit disconnectToReadyRead();
  m_ifudpLogging = 0;
}

void Plots::settings() {
  if (m_ifudpLogging) {
    QMessageBox msgBox;
    msgBox.setText("Please Stop UDP Logging");
    msgBox.exec();

    return;
  }

  Settings settings{
      .projectName = m_project_name,
      .exportFilePath = m_data_export_path,
      .plotBufferSize = m_data_buffers->bufferSize(),
      .refreshRate = m_refresh_rate,
      .hostaddress = m_hostaddress,
      .port = m_port,
      .udpBufferSize = m_udp_buffer_size,
      .useDataCount = m_use_data_count,
      .sourcePath = m_signals->sourcePath(),
      .relativeHeaderPath = m_signals->relativeHeaderPath(),
      .additionalIncludes = m_signals->additionalIncludes(),
  };

  SettingsDialog dialog(m_settings_filename, this);
  dialog.setSettings(settings);
  if (dialog.exec() == QDialog::Accepted) {
    Settings settings;
    dialog.settings(settings);

    m_project_name = settings.projectName;
    m_data_export_path = settings.exportFilePath;

    emit plotBufferSizeChanged(settings.plotBufferSize);
    m_refresh_rate = settings.refreshRate;

    m_hostaddress = settings.hostaddress;
    m_port = settings.port;
    m_udp_buffer_size = settings.udpBufferSize;
    m_use_data_count = settings.useDataCount;

    m_signals->setSignalSettings(settings.sourcePath,
                                 settings.relativeHeaderPath,
                                 settings.additionalIncludes);

    emit initUDP(
        m_hostaddress, static_cast<quint16>(m_port), m_udp_buffer_size,
        m_refresh_rate, m_use_data_count,
        dialog.createExportPath(m_settings_filename, m_data_export_path),
        m_project_name);
  }
}

void Plots::showInfoMessageBox(const QString title, const QString text) const {
  QMessageBox msgBox(m_parent);
  msgBox.setWindowTitle(title);
  msgBox.setText(text);
  msgBox.exec();
}

void Plots::clearPlots() {
  m_mutex->lock();
  m_data_buffers->clearPlots();
  m_mutex->unlock();
  for (int i = 0; i < m_plots.length(); i++) {
    m_plots[i]->replot();
  }
}

Plots::~Plots() {

  int size = m_plots.length();
  for (int i = 0; i < size; i++) {
    deletePlot(m_plots.at(0));
  }
  m_udp_thread.quit();
  m_udp_thread.wait(); // waiting till thread quits
  delete m_udp;
  delete m_mutex;
  delete m_data_buffers;
}

void Plots::setSettingsFilename(const QString& filename) {
  m_settings_filename = filename;
  emit initUDP(
      m_hostaddress, static_cast<quint16>(m_port), m_udp_buffer_size,
      m_refresh_rate, m_use_data_count,
      SettingsDialog::createExportPath(m_settings_filename, m_data_export_path),
      m_project_name);
}
