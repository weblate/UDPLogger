#ifndef TRIGGERWIDGET_H
#define TRIGGERWIDGET_H

#include <QWidget>

namespace Ui {
class TriggerWidget;
}
class Signals;

enum TriggerType {
  RISING_EDGE,
  FALLING_EDGE,
  ALL_EDGES,
  COUNT_TRIGGER_TYPES // all new types must be before this
};

struct TriggerSettings {
  bool enabled{false};
  double timeAfterTrigger{0};
  double timeBeforeTrigger{0};
  TriggerType type{TriggerType::RISING_EDGE};
  double level{0};
  int signalIndex{0};
  bool automaticRestart{false};

  bool operator==(const TriggerSettings& other) const {
    return (enabled == other.enabled &&
            timeAfterTrigger == other.timeAfterTrigger &&
            timeBeforeTrigger == other.timeBeforeTrigger &&
            type == other.type && level == other.level &&
            signalIndex == other.signalIndex &&
            automaticRestart == other.automaticRestart);
  }
};

const QString trigger_types[TriggerType::COUNT_TRIGGER_TYPES] = {
    "RISING EDGE", "FALLING EDGE", "ALL EDGES"};

class TriggerWidget : public QWidget {
  Q_OBJECT

public:
  explicit TriggerWidget(Signals* signal, QWidget* parent = nullptr);
  void fromJsonObject(const QJsonObject& obj);
  QJsonObject toJsonObject() const;
  ~TriggerWidget();
  bool ifRestartTrigger() const;
  void disableTrigger();
  TriggerSettings settings() { return m_settings; }
public slots:
  void updateSignals();
  void triggered();
  void newTriggerValue(double value);
  void triggerFinished();
  void triggerStarted();

private slots:
  void triggerLevelChanged(double level);

  void triggerTypeChanged(int type);

  void signalIndexChanged(int);

  void triggerEnableChanged(bool checked);

  void triggerRestartChanged(bool checked);

  void triggerTimeBeforeChanged(double);

  void triggerTimeAfterChanged(double);

private:
  void setEnabled(bool);
signals:
  void startTrigger() const;
  void settingsChanged(const TriggerSettings&);

private:
  Ui::TriggerWidget* ui;
  Signals* m_signals{nullptr};
  TriggerSettings m_settings;

  friend class TestImportExport;
  friend class TestTriggerWidget;
  friend class TestMainWindow;
};

#endif // TRIGGERWIDGET_H
