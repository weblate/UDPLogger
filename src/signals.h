/***
 *  This file is part of UDPLogger
 *
 *  Copyright (C) 2018 Martin Marmsoler, martin.marmsoler at gmail.com
 *
 *  UDPLogger is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with UDPLogger.  If not, see <http://www.gnu.org/licenses/>.
 ***/

#ifndef SIGNALS_H
#define SIGNALS_H
#include <QString>
#include <QVector>
#include <QObject>

struct Signal {
  QString name;
  QString datatype;
  QString unit;
  QString struct_name;

  static Signal fromJsonObject(const QJsonObject& obj);
  QJsonObject toJsonObject() const;

  // internal
  int index;
  int startByte;

  bool isValid() { return !name.isEmpty(); }
};

struct input_arguments {
  QString datatype;
  QString variable_name;
};

// eventuell erweitern, dass signalnamen von datei eingelesen werden können.
class Signals : public QObject {
  Q_OBJECT
public:
  Signals();
  int getSignalCount() const { return m_signals.length(); }
  Signal getSignal(int index) const { return m_signals[index]; }
  Signal getSignal(const QString& name) const;
  void setSignals(QVector<Signal>& imported_signals);
  int importJSonFile(const QString& filename);
#ifdef HAVE_XLNT
  int importXLSX(const QString& filename);
#endif
  int importXML(const QString& filename);
  bool isStruct(const QString& variablename) const;
  static QString validateDatatype(const QString& datatype);
  int parseJsonObject(const QJsonObject& object);
  void writeToJsonObject(QJsonObject& object) const;
  int calculateMinBufferLength() const;
  QString
  ifConditionBuffersize(const input_arguments& buffer_length_variable) const;
  int determineDatatypeSize(const QString& datatype) const;
  void setSignalSettings(const QString& sourcePath,
                         const QString& relative_header_path,
                         const QString& additional_includes);
  QString sourcePath() const { return m_c_path; }
  QString relativeHeaderPath() const { return m_header_path; }
  QString additionalIncludes() const { return m_additional_includes; }

private:
  bool ifStructNameExist(const QVector<struct input_arguments>& arguments,
                         const QString& struct_name, bool& ifstruct) const;
  void getInputArguments(QVector<struct input_arguments>& arguments) const;
  void createMemcpyStrings(QVector<QString>& memcpy_strings) const;

public slots:
  int importSignals();
  void exportUDPFunction();
  bool signalExist(const Signal& signal_to_match) const;

signals:
  void aboutToChangeSignals();
  void signalsChanged();
  void importFailed(const QString& title, const QString& text);
  void invalidSignal(const QString& message);

private:
  QVector<struct Signal> m_signals;
  QString m_header_path;
  QString m_additional_includes;
  QString m_c_path{"/home"};
};

#endif // SIGNALS_H
