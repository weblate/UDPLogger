/***
 *  This file is part of UDPLogger
 *
 *  Copyright (C) 2018 Martin Marmsoler, martin.marmsoler at gmail.com
 *
 *  UDPLogger is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with UDPLogger.  If not, see <http://www.gnu.org/licenses/>.
 ***/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "plots.h"

#include <iostream>
#include "signals.h"
#include <QDockWidget>
#include "triggerwidget.h"
#include "signalsimportdialog.h"
#include "guisettingsdialog.h"
#include "settings.h"

using namespace std;

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);

  m_vlayout = new QVBoxLayout(this);

  m_signal = new Signals;

  m_trigger_dock = new QDockWidget(tr("Store signals to file trigger"), this);
  connect(m_trigger_dock, &QDockWidget::visibilityChanged, this,
          &MainWindow::changedDockVisibility);
  m_triggerwidget = new TriggerWidget(m_signal, m_trigger_dock);
  connect(m_signal, &Signals::signalsChanged, m_triggerwidget,
          &TriggerWidget::updateSignals);
  m_trigger_dock->setWidget(m_triggerwidget);
  addDockWidget(Qt::RightDockWidgetArea, m_trigger_dock);
  m_trigger_dock->show();

  readSettings();

  m_plots = new Plots(ui->scrollArea, m_signal, m_triggerwidget);
  connect(this, &MainWindow::settingsFilenameChanged, m_plots,
          &Plots::setSettingsFilename);
  ui->scrollArea->setWidget(m_plots);
  QMenu* menu = ui->menuBar->addMenu(tr("&File"));

  QAction* save_settings_as = new QAction(tr("&save Settings as ..."), this);
  save_settings_as->setStatusTip(tr("Wizard to create a new Plot"));
  connect(save_settings_as, &QAction::triggered, this,
          &MainWindow::saveSettingsAs);
  menu->addAction(save_settings_as);

  QAction* save_settings = new QAction(tr("&save Settings"), this);
  save_settings->setStatusTip(tr("Wizard to create a new Plot"));
  connect(save_settings, &QAction::triggered, this, &MainWindow::saveSettings);
  menu->addAction(save_settings);

  QAction* import_settings = new QAction(tr("&import Settings"), this);
  import_settings->setStatusTip(tr("Wizard to create a new Plot"));
  connect(import_settings, &QAction::triggered, this,
          qOverload<>(&MainWindow::importSettings));
  menu->addAction(import_settings);

  QAction* import_signals = new QAction(tr("&import Signals"), this);
  import_signals->setStatusTip(tr("Import new Signals from file"));
  connect(import_signals, &QAction::triggered, this,
          &MainWindow::importSignals);
  menu->addAction(import_signals);

  QAction* export_function =
      new QAction(tr("Export C/C++ Package function"), this);
  connect(export_function, &QAction::triggered, m_signal,
          &Signals::exportUDPFunction);
  menu->addAction(export_function);

  QAction* addPlot = new QAction(tr("&Add Plot"), this);
  addPlot->setStatusTip(tr("Wizard to create a new Plot"));
  connect(addPlot, &QAction::triggered, m_plots, &Plots::createNewPlot);
  ui->menuBar->addAction(addPlot);

  m_start_udp = new QAction(tr("Start UDP"), this);
  m_start_udp->setStatusTip(tr("Starts reading from UDP Buffer"));
  connect(m_start_udp, &QAction::triggered, m_plots, &Plots::startUDP);
  connect(m_start_udp, &QAction::triggered, this, &MainWindow::disableStartUDP);
  ui->menuBar->addAction(m_start_udp);

  m_stop_udp = new QAction(tr("Stop UDP"), this);
  m_stop_udp->setStatusTip(tr("Stops reading from UDP Buffer"));
  connect(m_stop_udp, &QAction::triggered, m_plots, &Plots::stopUDP);
  connect(m_stop_udp, &QAction::triggered, this, &MainWindow::disableStopUDP);
  ui->menuBar->addAction(m_stop_udp);

  QAction* clearPlotBuffer = new QAction(tr("Clear plot buffer"), this);
  clearPlotBuffer->setStatusTip(
      tr("Clearing the plot buffer to plot new data, where the keys are "
         "smaller than the last keys."));
  connect(clearPlotBuffer, &QAction::triggered, m_plots, &Plots::clearPlots);
  ui->menuBar->addAction(clearPlotBuffer);

  QAction* project_settings = new QAction(tr("Project Settings"), this);
  connect(project_settings, &QAction::triggered, m_plots, &Plots::settings);
  ui->menuBar->addAction(project_settings);

  QAction* settings = new QAction(tr("Settings"), this);
  connect(settings, &QAction::triggered, this, &MainWindow::openSettings);
  ui->menuBar->addAction(settings);

  // Begin Menu "View"
  QMenu* view_menu = ui->menuBar->addMenu(tr("View"));

  m_show_trigger_dock = new QAction(tr("Trigger dock"), this);
  m_show_trigger_dock->setCheckable(true);
  m_show_trigger_dock->setChecked(true);
  view_menu->addAction(m_show_trigger_dock);
  connect(m_show_trigger_dock, &QAction::toggled, this,
          &MainWindow::changeDockVisibility);

  disableStopUDP();
}

void MainWindow::openSettings() {
  Settings s;
  auto* d =
      new GuiSettingsDialog(s.value(SettingsKey::language).toString(), this);

  connect(d, &QDialog::finished, [d](int result) {
    if (result == QDialog::Accepted) {
      Settings s;
      s.setValue(SettingsKey::language, d->language());
    }
    d->deleteLater();
  });

  d->show();
}

void MainWindow::setSettingsFilename(const QString& filename) {
  m_settings_filename = filename;
  emit settingsFilenameChanged(m_settings_filename);
}

void MainWindow::saveSettings() {
  if (m_settings_filename.compare("") == 0) {
    saveSettingsAs();
    return;
  }

  exportSettings(m_settings_filename);
}

void MainWindow::saveSettingsAs() {
  QString select = tr("UDP Logger Config Files (*.udpLoggerSettings)");
  QFileDialog filedialog;
  filedialog.setDefaultSuffix(".udpLoggerSettings");
  QString filename = filedialog.getSaveFileName(
      this, tr("Export Settings"), m_settings_filename, select, &select);

  if (filename.compare("") == 0)
    return;

  setSettingsFilename(filename);

  exportSettings(filename);
}

void MainWindow::exportSettings(const QString& filename) {
  QFile saveFile(filename);

  if (!saveFile.open(QIODevice::WriteOnly)) {
    qWarning("Couldn't open save file.");
    // return false;
  }

  auto object = m_plots->toJsonObject();
  object["TriggerSettings"] = m_triggerwidget->toJsonObject();

  QJsonDocument document(object);
  saveFile.write(document.toJson());
  saveFile.close();
}

int MainWindow::importSettings() {

  if (m_plots->udpLogging()) {
    QMessageBox msgBox;
    msgBox.setText(tr("Please Stop UDP Logging"));
    msgBox.exec();

    return -1;
  }

  QString filename = QFileDialog::getOpenFileName(
      this, tr("Open settingsfile"), m_settings_filename,
      tr("UDP Logger Config Files (*.udpLoggerSettings)"));

  setSettingsFilename(filename);
  return importSettings(filename);
}

int MainWindow::importSettings(const QString& fileName) {
  if (fileName.compare("") == 0)
    return -2;

  QFile file;
  file.setFileName(fileName);
  if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    return -1;
  QString val = file.readAll();
  file.close();
  QJsonDocument d = QJsonDocument::fromJson(val.toUtf8());

  QJsonObject object = d.object();

  m_plots->fromJsonObject(object);
  m_triggerwidget->fromJsonObject(object);

  return 0;
}

void MainWindow::importSignals() {
  SignalsImportDialog dialog(m_signal, this);
  dialog.exec();
}

void MainWindow::disableStartUDP() {
  m_start_udp->setEnabled(false);
  m_stop_udp->setEnabled(true);
}

void MainWindow::disableStopUDP() {
  m_start_udp->setEnabled(true);
  m_stop_udp->setEnabled(false);
}

void MainWindow::writeSettings() {
  Settings s;
  s.beginGroup("mainWindow");
  s.setValue("geometry", saveGeometry());
  s.setValue("state", saveState());
  s.endGroup();
}

void MainWindow::readSettings() {
  Settings s;
  s.beginGroup("mainWindow");
  restoreGeometry(s.value("geometry").toByteArray());
  restoreState(s.value("state").toByteArray());
  s.endGroup();
}

MainWindow::~MainWindow() {
  writeSettings();
  delete m_plots;
  delete m_signal;
  delete ui;
}

void MainWindow::changeDockVisibility(bool checked) {
  Q_UNUSED(checked);
  m_trigger_dock->setVisible(m_show_trigger_dock->isChecked());
}

void MainWindow::changedDockVisibility(bool visible) {
  Q_UNUSED(visible);
  m_show_trigger_dock->setChecked(m_trigger_dock->isVisible());
}

void MainWindow::showInfoMessageBox(const QString& title, const QString& text) {
  QMessageBox msgBox(this);
  msgBox.setWindowTitle(title);
  msgBox.setText(text);
  msgBox.exec();
}
