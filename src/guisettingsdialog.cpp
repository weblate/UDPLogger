#include "guisettingsdialog.h"
#include "ui_guisettingsdialog.h"

namespace {
const QHash<QString, QString> languages = {
    {QObject::tr("System"), "system"},
    {QObject::tr("Brézilien"), "pt_br"},
    {QObject::tr("Catalan"), "ca"},
    {QObject::tr("Tchèque"), "cs"},
    {QObject::tr("Allemand"), "de"},
    {QObject::tr("Danois"), "da"},
    {QObject::tr("Grec"), "el"},
    {QObject::tr("Anglais"), "en"},
    {QObject::tr("Espagnol"), "es"},
    {QObject::tr("Français"), "fr"},
    {QObject::tr("Croate"), "hr"},
    {QObject::tr("Italien"), "it"},
    {QObject::tr("Japonais"), "ja"},
    {QObject::tr("Polonais"), "pl"},
    {QObject::tr("Portugais"), "pt"},
    {QObject::tr("Roumains"), "ro"},
    {QObject::tr("Russe"), "ru"},
    {QObject::tr("Slovène"), "sl"},
    {QObject::tr("Pays-Bas"), "nl"},
    {QObject::tr("Norvege"), "nb"},
    {QObject::tr("Belgique-Flemish"), "be"},
    {QObject::tr("Hongrois"), "hu"},
    {QObject::tr("Mongol"), "mn"},
};
}

GuiSettingsDialog::GuiSettingsDialog(const QString& currLang, QWidget* parent)
    : QDialog(parent), ui(new Ui::GuiSettingsDialog) {
  ui->setupUi(this);

  QHashIterator<QString, QString> i(languages);
  int currLangIndex = 0;
  int counter = 0;
  while (i.hasNext()) {
    i.next();
    ui->cb_language->addItem(i.key(), i.value());
    if (i.value() == currLang)
      currLangIndex = counter;
    counter++;
  }
  ui->cb_language->setCurrentIndex(currLangIndex);
}

QString GuiSettingsDialog::language() const {
  const QString lang = ui->cb_language->currentData().toString();
  return lang;
}

GuiSettingsDialog::~GuiSettingsDialog() { delete ui; }
