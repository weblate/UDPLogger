#ifndef PLOTBUFFER_H
#define PLOTBUFFER_H

#include <QObject>
#include <qcustomplot.h>

#include "signals.h"

/*!
 * \brief The PlotBuffer class
 * Data container
 */
class PlotBuffer : public QCPGraphDataContainer {
public:
  PlotBuffer();
  PlotBuffer(int index_x, int index_y);
  inline int getIndexX() const { return m_index_x; }
  inline int getIndexY() const { return m_index_y; }
  inline void increaseCounter() { m_count++; }
  int decreaseCounter();

private:
  int m_index_x; // x axis signal index
  int m_index_y; // y axis signal index
  int m_count;
  int m_xmin, m_xmax, m_ymin, m_ymax;
};

class PlotBuffers : public QObject {
  Q_OBJECT
public:
  PlotBuffers(Signals* signal);
  ~PlotBuffers();
  QSharedPointer<QCPGraphDataContainer> getBuffer(const Signal& xaxis,
                                                  const Signal& yaxis);
  int bufferSize() { return m_plot_buffer_size; }
  void removeGraph(const Signal& xaxis, const Signal& yaxis);

  /*!
   * \brief addData
   * New data arrived. Add the data to the corresponding buffers
   * \param data
   * \param length
   */
  void addData(const char* data, int length);

  template <typename T> static inline T get(int position, const char* data) {
    return *((T*)(&data[position]));
  }
  static inline float getFloat(int position, const char* data) {
    return get<float>(position, data);
  }
  static inline double getDouble(int position, const char* data) {
    return get<double>(position, data);
  }
  static inline int16_t getShort(int position, const char* data) {
    return get<int16_t>(position, data);
  }
  static inline uint16_t getUShort(int position, const char* data) {
    return get<uint16_t>(position, data);
  }
  static inline char getChar(int position, const char* data) {
    return get<char>(position, data);
  }
  static inline uint8_t getUChar(int position, const char* data) {
    return get<uint8_t>(position, data);
  }
  static inline bool getBool(int position, const char* data) {
    return get<bool>(position, data);
  }
  static inline int32_t getInt(int position, const char* data) {
    return get<int32_t>(position, data);
  }
  static inline uint32_t getUInt(int position, const char* data) {
    return get<uint32_t>(position, data);
  }
  static inline int64_t getInt64(int position, const char* data) {
    return get<int64_t>(position, data);
  }
  static inline uint64_t getUInt64(int position, const char* data) {
    return get<uint64_t>(position, data);
  }
  /*!
   * \brief getValue
   * Get double value of \p signal from \p data. Length is the max datalenght of
   * \p data To prevent a buffer overflow \param data \param length \param
   * signal \return
   */
  static double getValue(const char* data, int length, const Signal& signal);
  void clearPlots();
public slots:
  void setPlotBufferSize(int plot_buffer_size);
  void removeSignal(const Signal& xaxis, const Signal& yaxis);
signals:
  void dataChanged();

private:
  QVector<QSharedPointer<PlotBuffer>> m_plot_buffer;
  Signals* m_signals;
  int m_plot_buffer_size{200};

  friend class TestUDP;
};

#endif // PLOTBUFFER_H
