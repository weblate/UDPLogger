#ifndef SIGNALSIMPORTDIALOG_H
#define SIGNALSIMPORTDIALOG_H

#include <QDialog>

class Signals;

namespace Ui {
class SignalsImportDialog;
}

class SignalsImportDialog : public QDialog {
  Q_OBJECT

public:
  explicit SignalsImportDialog(Signals* s, QWidget* parent = nullptr);
  ~SignalsImportDialog();
  void addMessage(const QString& message);
  void importFailed(const QString& title, const QString& description);

private:
  Ui::SignalsImportDialog* ui;
};

#endif // SIGNALSIMPORTDIALOG_H
