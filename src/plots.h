/***
 *  This file is part of UDPLogger
 *
 *  Copyright (C) 2018 Martin Marmsoler, martin.marmsoler at gmail.com
 *
 *  UDPLogger is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with UDPLogger.  If not, see <http://www.gnu.org/licenses/>.
 ***/

#ifndef PLOTS_H
#define PLOTS_H

#include "signals.h"
#include <array>
#include <QHostAddress>
#include <QtWidgets>
#include "plotbuffer.h"

class UDP;
class QMutex;
class QJsonObject;
class SettingsDialog;
class Plot;
class TriggerWidget;

class Plots : public QWidget {
  Q_OBJECT

public:
  Plots(QWidget* parent, Signals* signal, TriggerWidget* trigger);
  QVBoxLayout* getLayout() const { return m_layout; }
  Plot* createNewPlot();
  ~Plots();
  void removeGraph(const Signal& xaxis, const Signal& yaxis);
  QSharedPointer<QCPGraphDataContainer> getBuffer(const Signal& xaxis,
                                                  const Signal& yaxis);
  QWidget* getParent() const { return m_parent; }
  bool udpLogging() { return m_ifudpLogging; }
public slots:
  void deletePlot(Plot* plot_address);
  QJsonObject toJsonObject();
  int fromJsonObject(const QJsonObject&);
  void settings();
  void startUDP();
  void stopUDP();
  // Do not use references here, because it will be called from another thread
  void showInfoMessageBox(const QString title, const QString text) const;
  void clearPlots();
  void setSettingsFilename(const QString&);
signals:
  void startUDPReadData();
  void connectToReadyRead();
  void disconnectToReadyRead();
  void initUDP(const QHostAddress& hostaddress, const quint16 port,
               const int udp_buffer_size, const int refresh_rate,
               const int m_use_data_count, const QString& export_path,
               const QString& project_name);
  void changeSignalSettings(const QString& sourcePath,
                            const QString& relative_header_path,
                            const QString& additional_includes);
  void plotBufferSizeChanged(const int data_buffer_size);
  void
  settingsFileNameChanged(const QString); // Do not use references, because we
                                          // are talking to another thread

private:
  QWidget* m_parent;
  QVector<Plot*> m_plots;
  QVBoxLayout* m_layout;
  Signals* m_signals;
  QThread m_udp_thread;
  UDP* m_udp;
  QMutex* m_mutex;
  QHostAddress m_hostaddress{QHostAddress::Any};
  int m_port{60000};
  int m_udp_buffer_size{400};
  int m_refresh_rate{60};
  unsigned long m_index_new_data{
      0}; // used with m_refresh_rate // TODO: internal?
  int m_use_data_count{0};
  QString m_data_export_path;

  QString m_project_name{"NewProject"};
  bool m_ifudpLogging{false};

  PlotBuffers* m_data_buffers;

  /*!
   * \brief m_settings_filename
   * Filename of the settingsfile. Used to determine the export path
   * if m_data_export_path is relative
   */
  QString m_settings_filename;

  friend class TestPlots;
  friend class TestMainWindow;
};

#endif // PLOTS_H
