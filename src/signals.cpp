/***
 *  This file is part of UDPLogger
 *
 *  Copyright (C) 2018 Martin Marmsoler, martin.marmsoler at gmail.com
 *
 *  UDPLogger is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with UDPLogger.  If not, see <http://www.gnu.org/licenses/>.
 ***/

#include "signals.h"
#include <QFile>
#include <QFileDialog>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QDebug>
#include <QXmlStreamReader>
#ifdef HAVE_XLNT
#include <xlnt/xlnt.hpp>
#endif
#include <iostream>
#include <algorithm>

namespace JsonSections {
const QString datatype = "Datatype";
const QString signal_name = "SignalName";
const QString struct_name = "StructName";
const QString unit = "Unit";

const QString signals_ = "Signals";
const QString export_source_path = "ExportSourcePath";
const QString relative_header_path = "RelativeHeaderPath";
const QString additional_includes = "AdditionalIncludes";
} // namespace JsonSections

Signal Signal::fromJsonObject(const QJsonObject& obj) {
  return {.name = obj[JsonSections::signal_name].toString(),
          .datatype =
              Signals::validateDatatype(obj[JsonSections::datatype].toString()),
          .unit = obj[JsonSections::unit].toString(),
          .struct_name = obj[JsonSections::struct_name].toString()};
}

QJsonObject Signal::toJsonObject() const {
  QJsonObject obj;
  obj[JsonSections::datatype] = datatype;
  obj[JsonSections::signal_name] = name;
  obj[JsonSections::struct_name] = struct_name;
  obj[JsonSections::unit] = unit;
  return obj;
}

Signals::Signals() {}

int Signals::importSignals() {
  QString signalfileEnding = "*.udpLoggerSignals";
#ifdef HAVE_XLNT
  signalfileEnding += " *.xlsx";
#endif
  signalfileEnding += " *.xml";
  QString fileName =
      QFileDialog::getOpenFileName(nullptr, tr("Open Signals file"), "/home",
                                   tr("SignalFiles(%1)").arg(signalfileEnding));
  if (fileName.compare("") == 0)
    return -1;

  int error;
  if (fileName.split(".").last() == "xml")
    error = importXML(fileName);
#ifdef HAVE_XLNT
  else if (fileName.split(".").last() == "xlsx")
    error = importXLSX(fileName);
#endif
  else
    error = importJSonFile(fileName);
  return error;
}

int Signals::importXML(const QString& filename) {
  QFile file(filename);
  if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    return -1;

  QXmlStreamReader reader(&file);
  QVector<Signal> signal_vector;
  bool signalsFound = false;
  int signalCounter = 0;
  while (!reader.atEnd()) {
    reader.readNext();
    if (!reader.isStartElement())
      continue;

    const QStringRef n = reader.name();
    if (n.compare(QStringLiteral("Signals")) == 0) {
      signalsFound = true;
      continue;
    } else if (!signalsFound)
      continue;

    if (n.compare(QStringLiteral("Signal")) != 0)
      continue;

    signalCounter++;

    QString name;
    QString structname;
    QString unit;
    QString datatype;

    const auto attributes = reader.attributes();

    auto str = attributes.value(QStringLiteral("name")).toString();
    if (str.isEmpty()) {
      qDebug() << "Name attribute not found";
      emit invalidSignal(tr("Signal %1: Name attribute not found")
                             .arg(QString::number(signalCounter)));
      continue;
    } else
      name = str;

    structname = attributes.value(QStringLiteral("structname")).toString();
    unit = attributes.value(QStringLiteral("unit")).toString();

    str = attributes.value(QStringLiteral("datatype")).toString();
    if (str.isEmpty()) {
      qDebug() << "Datatype attribute not found";
      emit invalidSignal(tr("Signal %1: Datatype attribute not found")
                             .arg(QString::number(signalCounter)));
      continue;
    }

    str = validateDatatype(str);
    if (str.isEmpty()) {
      qDebug() << "Datatype attribute is invalid";
      emit invalidSignal(tr("Signal %1: Datatype attribute is invalid")
                             .arg(QString::number(signalCounter)));
      continue;
    } else
      datatype = str;

    Signal signal_element;
    signal_element.datatype = datatype;
    signal_element.name = name;
    signal_element.unit = unit;
    signal_element.struct_name = structname;
    signal_vector.append(signal_element);
  }
  setSignals(signal_vector);
  return 0;
}

#ifdef HAVE_XLNT
int Signals::importXLSX(const QString& filename) {

  QVector<Signal> new_signals;

  xlnt::workbook wb;
  wb.load(filename.toStdString());
  auto ws = wb.active_sheet();

  // read in the complete spreadsheet
  std::vector<std::vector<std::string>> theWholeSpreadSheet;
  for (auto row : ws.rows(false)) {
    std::vector<std::string> aSingleRow;
    for (auto cell : row) {
      aSingleRow.push_back(cell.to_string());
    }
    theWholeSpreadSheet.push_back(aSingleRow);
  }

  // Find header row
  long description_row = -1;
  QString index_column_string = "Signalname";
  for (unsigned long i = 0; i < theWholeSpreadSheet.size(); i++) {
    auto row = theWholeSpreadSheet.at(i);
    QString column_null = QString::fromStdString(row[0]);
    if (column_null.compare(index_column_string) == 0) {
      description_row = i;
    }
  }

  if (description_row < 0) {
    importFailed(tr("No description row found"),
                 tr("No row found, where in the first column is the text '%1'.")
                     .arg(index_column_string));
    return -1;
  }

  // Find columns where the needed information is stored
  uint64_t datatype_column = -1;
  const QString datatype_column_string = "Datatype";
  uint64_t name_column = -1;
  const QString name_column_string = "Signalname";
  uint64_t struct_name_column = -1;
  const QString struct_name_column_string = "Structname";
  uint64_t unit_name_column = -1;
  const QString unit_name_column_string = "Unit";

  for (unsigned long column = 0;
       column < theWholeSpreadSheet.at(description_row).size(); column++) {
    QString column_value = QString::fromStdString(
        theWholeSpreadSheet.at(description_row).at(column));
    if (column_value.compare(datatype_column_string) == 0) {
      datatype_column = column;
      continue;
    }

    if (column_value.compare(name_column_string) == 0) {
      name_column = column;
      continue;
    }

    if (column_value.compare(struct_name_column_string) == 0) {
      struct_name_column = column;
      continue;
    }
    if (column_value.compare(unit_name_column_string) == 0) {
      unit_name_column = column;
      continue;
    }
  }

  // Check if all columns are found
  const QString error = tr("Column not found");
  const QString description =
      tr("No column found with the name '%1'. Make sure, that this column name "
         "is in the same row as the Index description");
  if (datatype_column < 0) {
    importFailed(error, description.arg(datatype_column_string));
    return -2;
  } else if (name_column < 0) {
    importFailed(error, description.arg(name_column_string));
    return -2;
  }
  // Those are optional
  /* else if (struct_name_column < 0) {
    showMessageBox(error, description.arg(struct_name_column_string));
    return -2;
  } else if (unit_name_column < 0) {
    showMessageBox(error, description.arg(unit_name_column_string));
    return -2;
  } */

  bool success = true;
  for (unsigned long i = description_row + 1; i < theWholeSpreadSheet.size();
       i++) {
    Signal signal;
    const QString datatype =
        QString::fromStdString(theWholeSpreadSheet.at(i).at(datatype_column));
    signal.datatype = validateDatatype(datatype);
    signal.name =
        QString::fromStdString(theWholeSpreadSheet.at(i).at(name_column));
    if (signal.name.isEmpty()) {
      emit invalidSignal(
          tr("Signal %1: Name is invalid").arg(i - description_row));
      continue;
    }
    if (signal.datatype.isEmpty()) {
      success = false;
      emit invalidSignal(tr("Signal %1: Datatype is invalid")
                             .arg(QString::number(i - description_row)));
      continue;
    }
    if (unit_name_column >= 0) {
      signal.unit = QString::fromStdString(
          theWholeSpreadSheet.at(i).at(unit_name_column));
    }
    if (struct_name_column >= 0) {
      signal.struct_name = QString::fromStdString(
          theWholeSpreadSheet.at(i).at(struct_name_column));
    }
    new_signals.append(signal);
  }
  if (success)
    setSignals(new_signals);
  return 0;
}
#endif

int Signals::importJSonFile(const QString& filename) {

  QFile file;
  file.setFileName(filename);
  file.open(QIODevice::ReadOnly | QIODevice::Text);
  QString val = file.readAll();
  file.close();
  QJsonDocument d = QJsonDocument::fromJson(val.toUtf8());

  QJsonObject object = d.object();
  return parseJsonObject(object);
}

void Signals::writeToJsonObject(QJsonObject& object) const {
  QJsonArray active_signals;

  for (const auto& signal : m_signals)
    active_signals.append(signal.toJsonObject());

  object[JsonSections::signals_] = active_signals;
  object[JsonSections::export_source_path] = m_c_path;
  object[JsonSections::relative_header_path] = m_header_path;
  object[JsonSections::additional_includes] = m_additional_includes;
}

int Signals::parseJsonObject(const QJsonObject& object) {
  int error = 0;
  m_additional_includes = object[JsonSections::additional_includes].toString();
  m_header_path = object[JsonSections::relative_header_path].toString();
  if (object.contains(JsonSections::signals_)) {
    QJsonArray imported_signals = object[JsonSections::signals_].toArray();
    QVector<Signal> signal_vector;
    for (int i = 0; i < imported_signals.count(); i++) {
      Signal signal_element =
          Signal::fromJsonObject(imported_signals[i].toObject());
      signal_vector.append(signal_element);
      if (signal_element.datatype.isEmpty()) {
        error = -1;
        emit importFailed(tr("No valid datatype"),
                          QString(tr("The datatype of '%1' (%2) is not "
                                     "valid! \n No signals changed."))
                              .arg(signal_element.name)
                              .arg(signal_element.datatype));
        break;
      }
    }
    if (error == 0) {
      setSignals(signal_vector);
    }
  }

  if (object.contains(JsonSections::export_source_path)) {
    m_c_path = object[JsonSections::export_source_path].toString();
  }
  return error;
}

Signal Signals::getSignal(const QString& name) const {
  for (auto& signal : m_signals) {
    if (signal.name.compare(name) == 0)
      return signal;
  }
  return Signal();
}

void Signals::setSignals(QVector<Signal>& imported_signals) {
  // TODO: connect to this signal
  emit aboutToChangeSignals();

  // asign startByte
  int startByte = 0;
  for (int i = 0; i < imported_signals.length(); i++) {
    imported_signals[i].index = i;
    imported_signals[i].startByte = startByte;
    startByte += determineDatatypeSize(imported_signals[i].datatype);
  }

  m_signals = imported_signals;
  emit signalsChanged();
}

int Signals::determineDatatypeSize(const QString& datatype) const {
  if (datatype.compare("int8_t") == 0)
    return 1;
  if (datatype.compare("uint8_t") == 0)
    return 1;
  if (datatype.compare("bool") == 0)
    return 1;
  if (datatype.compare("int16_t") == 0)
    return 2;
  if (datatype.compare("uint16_t") == 0)
    return 2;
  if (datatype.compare("int32_t") == 0)
    return 4;
  if (datatype.compare("uint32_t") == 0)
    return 4;
  if (datatype.compare("int64_t") == 0)
    return 8;
  if (datatype.compare("uint64_t") == 0)
    return 8;
  if (datatype.compare("float") == 0)
    return 4;
  if (datatype.compare("double") == 0)
    return 8;
  return 0;
}

QString Signals::validateDatatype(const QString& datatype) {
  if (datatype.compare("char") == 0 || datatype.compare("int8_t") == 0) {
    return "int8_t";
  }
  if (datatype.compare("uint8_t") == 0) {
    return datatype;
  }
  if (datatype.compare("bool") == 0) {
    return datatype;
  }
  if (datatype.compare("int16_t") == 0) {
    return datatype;
  }
  if (datatype.compare("uint16_t") == 0) {
    return datatype;
  }
  if (datatype.compare("int") == 0 || datatype.compare("int32_t") == 0) {
    return "int32_t";
  }
  if (datatype.compare("uint32_t") == 0) {
    return datatype;
  }
  if (datatype.compare("uint64_t") == 0) {
    return datatype;
  }
  if (datatype.compare("int64_t") == 0) {
    return datatype;
  }
  if (datatype.compare("float") == 0) {
    return datatype;
  }
  if (datatype.compare("double") == 0) {
    return datatype;
  }

  return ""; // unsupported datatype detected
}

bool Signals::signalExist(const Signal& signal_to_match) const {
  for (const Signal& signal : m_signals) {
    //    bool datatype = signal.datatype.compare(signal_to_match.datatype) ==
    //    0;
    bool name = signal.name.compare(signal_to_match.name) == 0;
    //    bool unit = signal.unit == signal_to_match.unit;
    //    bool struct_name = signal.struct_name == signal_to_match.struct_name;
    if (name) {
      return 1;
    }
  }
  return 0;
}

void Signals::exportUDPFunction() {

  QString pathWithFileName =
      QFileDialog::getSaveFileName(nullptr, tr("Export C Funktion"), m_c_path,
                                   tr("C/C++ File (*.c, *.cpp)"));

  if (pathWithFileName.compare("") == 0) {
    return;
  }

  QStringList tempListPath = pathWithFileName.split("/");
  QString file_name = tempListPath.last();
  tempListPath.removeLast();
  QString path = tempListPath.join("/");
  m_c_path = pathWithFileName;

  QStringList tempList = file_name.split(".");
  QString fileNameHeader = "";
  QString fileNameDeclaration = "";

  if (tempList.length() == 1) {
    fileNameHeader = file_name + ".h";
    fileNameDeclaration = file_name + ".cpp";
  } else {
    fileNameDeclaration = file_name;
    for (int i = 0; i < tempList.length() - 1;
         i++) { // last element of tempList is extension
      fileNameHeader += tempList[i];
    }
    fileNameHeader += ".h";
  }

  QFile saveFileDefinition(path + "/" + m_header_path + "/" + fileNameHeader);
  if (!saveFileDefinition.open(QIODevice::WriteOnly)) {
    qWarning("Couldn't open header file.");
  }

  QFile saveFileDeclaration(path + "/" + fileNameDeclaration);
  if (!saveFileDeclaration.open(QIODevice::WriteOnly)) {
    qWarning("Couldn't open c/cpp file.");
  }

  saveFileDeclaration.write("// Automatically generated by UDPLogger.\n");

  QVector<struct input_arguments> arguments;
  QVector<QString> memcpy_variable;
  QString function_name = "createUDPPackage";
  QString function_return_value = "int";
  QVector<QString> includes;
  includes.append("#include <string.h>\n");
  includes.append(
      "#include \"" +
      fileNameHeader.split("/")[fileNameHeader.split("/").length() - 1] +
      "\"\n");
  if (!m_additional_includes.isEmpty()) {
    QStringList additional_includes = m_additional_includes.split(";");
    for (const QString& include : additional_includes)
      includes.append("#include \"" + include + "\"\n");
  }

  QByteArray array;
  // Header ifndef ...
  QString tempFileNameHeader = function_name.toUpper() + "_H";
  array.append(QString("#ifndef " + tempFileNameHeader + "\n").toUtf8());
  array.append(QString("#define " + tempFileNameHeader + "\n").toUtf8());
  saveFileDefinition.write(array);
  saveFileDefinition.write("#include <stdint.h>\n");

  // create arguments list
  struct input_arguments udp_buffer;
  udp_buffer.datatype = "char* ";
  udp_buffer.variable_name = "udp_buffer";
  arguments.append(udp_buffer);
  struct input_arguments buffer_length;
  buffer_length.datatype = "int";
  buffer_length.variable_name = "buffer_length";
  arguments.append(buffer_length);
  getInputArguments(arguments); // creates string of input arguments

  QString buffersize_condition = ifConditionBuffersize(buffer_length);

  createMemcpyStrings(memcpy_variable);

  for (auto include : includes)
    saveFileDeclaration.write(include.toUtf8());
  saveFileDeclaration.write("\n");

  array.clear();
  array.append(QString(function_return_value + " " + function_name).toUtf8());
  array.append("(");
  for (int i = 0; i < arguments.size(); i++) {
    struct input_arguments argument = arguments[i];

    QString const_prefix;
    if (i == 0) {
      const_prefix = "";
    } else {
      const_prefix = "const ";
    }
    array.append(
        QString(const_prefix + argument.datatype + " " + argument.variable_name)
            .toUtf8());
    if (i < arguments.size() - 1) {
      array.append(", ");
    }
  }
  array.append(")");
  saveFileDefinition.write(array +
                           QString(";\n").toUtf8()); // write definition to file
  array.append("{\n");

  saveFileDeclaration.write(array);

  saveFileDeclaration.write(buffersize_condition.toUtf8());

  for (auto memcpy : memcpy_variable) {
    saveFileDeclaration.write(QString("\t" + memcpy).toUtf8());
  }

  saveFileDeclaration.write("\treturn 0;\n"); // successfully packaged

  saveFileDeclaration.write("}");
  saveFileDeclaration.close();

  // Header File endif
  array.clear();
  array.append(QString("#endif //" + tempFileNameHeader).toUtf8());
  saveFileDefinition.write(array);
  saveFileDefinition.close();
}

QString Signals::ifConditionBuffersize(
    const input_arguments& buffer_length_variable) const {
  int length = calculateMinBufferLength(); // length in byte

  QString condition = "\tif(" + QString::number(length) + ">" +
                      buffer_length_variable.variable_name + "){\n";
  condition += "\t\t return -1;\n";
  condition += "\t}\n";
  return condition;
}

int Signals::calculateMinBufferLength() const {
  if (m_signals.isEmpty())
    return 0;

  const Signal& signal = m_signals.at(m_signals.length() - 1);

  int offset = signal.startByte + determineDatatypeSize(signal.datatype);
  return offset;
}

void Signals::createMemcpyStrings(QVector<QString>& memcpy_strings) const {
  QString prefix = "memcpy(";
  QString postfix = ");\n";

  memcpy_strings.append("char* pointer = udp_buffer;\n\n");

  QString memcpy;
  QString pointer;

  for (const auto& signal : m_signals) {
    pointer =
        "pointer = udp_buffer + " + QString::number(signal.startByte) + ";\n";
    memcpy_strings.append(pointer);
    if (!isStruct(signal.name)) {
      memcpy = prefix + "pointer, " + "&" + signal.name + ", sizeof(" +
               signal.name + ")" + postfix;
    } else {
      QString temp = signal.name;
      temp.replace(temp.indexOf("."), 1, "->");
      memcpy =
          prefix + "pointer, &" + temp + ", sizeof(" + temp + ")" + postfix;
    }
    memcpy_strings.append(memcpy);
  }
}

bool Signals::isStruct(const QString& variable_name) const {
  if (variable_name.split(".").size() > 1) {
    return true;
  }
  return false;
}

bool Signals::ifStructNameExist(const QVector<input_arguments>& arguments,
                                const QString& struct_name,
                                bool& ifstruct) const {
  for (const auto& argument : arguments) {
    if (argument.datatype.compare("struct " + struct_name + "*") == 0) {
      ifstruct = true;
      return 1;
    }
  }
  if (struct_name.compare("") == 0) {
    ifstruct = false;
    return 0;
  }
  ifstruct = true;
  return 0;
}

void Signals::getInputArguments(
    QVector<struct input_arguments>& arguments) const {
  // are variable with . in it's name --> struct
  struct input_arguments argument;
  for (const auto& signal : m_signals) {
    bool ifstruct;
    if (ifStructNameExist(arguments, signal.struct_name, ifstruct)) {
      continue;
    }

    if (ifstruct) {
      argument.datatype = "struct " + signal.struct_name + "*";
    } else {
      argument.datatype = signal.datatype;
    }
    argument.variable_name =
        signal.name.split(".")[0]; // first is the highest struct name
    arguments.append(argument);
  }
}

void Signals::setSignalSettings(const QString& sourcePath,
                                const QString& relative_header_path,
                                const QString& additional_includes) {
  m_c_path = sourcePath;
  m_header_path = relative_header_path;
  m_additional_includes = additional_includes;
}
