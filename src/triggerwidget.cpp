#include "triggerwidget.h"
#include "ui_triggerwidget.h"
#include "signals.h"

#include <QJsonObject>

namespace JsonSections {
const QString triggerEnabled = "Enabled";
const QString triggerAutomaticRestart = "AutomaticRestart";
const QString timeAfterTrigger = "TimeAfterTrigger";
const QString timeBeforeTrigger = "TimeBeforeTrigger";
const QString triggerType = "TriggerType";
const QString triggerLevel = "TriggerLevel";
const QString signalIndex = "SignalIndex";
const QString triggerSettings = "TriggerSettings";
} // namespace JsonSections

TriggerWidget::TriggerWidget(Signals* signal, QWidget* parent)
    : QWidget(parent), ui(new Ui::TriggerWidget), m_signals(signal) {
  ui->setupUi(this);

  for (int i = 0; i < TriggerType::COUNT_TRIGGER_TYPES; i++) {
    ui->cb_triggertype->addItem(trigger_types[i]);
  }

  ui->cb_triggertype->setCurrentIndex(0);
  ui->checkbox_enable_trigger->setEnabled(false);
  ui->lbl_status->setStyleSheet("QWidget { background-color: green }");

  connect(ui->checkbox_enable_trigger, &QCheckBox::clicked, this,
          &TriggerWidget::triggerEnableChanged);
  connect(ui->checkbox_restart_trigger, &QCheckBox::clicked, this,
          &TriggerWidget::triggerRestartChanged);
  connect(ui->pb_start_trigger, &QPushButton::pressed, this,
          &TriggerWidget::startTrigger);
  connect(ui->cb_triggertype, qOverload<int>(&QComboBox::currentIndexChanged),
          this, &TriggerWidget::triggerTypeChanged);
  connect(ui->spinbox_trigger_level,
          qOverload<double>(&QDoubleSpinBox::valueChanged), this,
          &TriggerWidget::triggerLevelChanged);
  connect(ui->spinbox_t_before_trigger,
          qOverload<double>(&QDoubleSpinBox::valueChanged), this,
          &TriggerWidget::triggerTimeBeforeChanged);
  connect(ui->spinbox_t_after_trigger,
          qOverload<double>(&QDoubleSpinBox::valueChanged), this,
          &TriggerWidget::triggerTimeAfterChanged);
  connect(ui->cb_signals, qOverload<int>(&QComboBox::currentIndexChanged), this,
          &TriggerWidget::signalIndexChanged);
}

TriggerWidget::~TriggerWidget() { delete ui; }

void TriggerWidget::fromJsonObject(const QJsonObject& obj) {
  if (obj.contains(JsonSections::triggerSettings)) {
    QJsonObject triggerSettings = obj[JsonSections::triggerSettings].toObject();

    m_settings.enabled = triggerSettings[JsonSections::triggerEnabled].toBool();
    m_settings.automaticRestart =
        triggerSettings[JsonSections::triggerAutomaticRestart].toBool();
    m_settings.timeAfterTrigger =
        triggerSettings[JsonSections::timeAfterTrigger].toInt();
    m_settings.timeBeforeTrigger =
        triggerSettings[JsonSections::timeBeforeTrigger].toInt();
    m_settings.level = triggerSettings[JsonSections::triggerLevel].toDouble();
    m_settings.type = static_cast<TriggerType>(
        triggerSettings[JsonSections::triggerType].toInt());
    m_settings.signalIndex = triggerSettings[JsonSections::signalIndex].toInt();
    emit settingsChanged(m_settings);
  }
}

QJsonObject TriggerWidget::toJsonObject() const {
  QJsonObject settings;
  settings[JsonSections::triggerEnabled] = m_settings.enabled;
  settings[JsonSections::triggerAutomaticRestart] = m_settings.automaticRestart;
  settings[JsonSections::timeAfterTrigger] = m_settings.timeAfterTrigger;
  settings[JsonSections::timeBeforeTrigger] = m_settings.timeBeforeTrigger;
  settings[JsonSections::triggerLevel] = m_settings.level;
  settings[JsonSections::triggerType] = m_settings.type;
  settings[JsonSections::signalIndex] = m_settings.signalIndex;

  return settings;
}

void TriggerWidget::updateSignals() {
  ui->cb_signals->clear();
  Signal signal_temp;
  for (int i = 0; i < m_signals->getSignalCount(); i++) {
    signal_temp = m_signals->getSignal(i);
    ui->cb_signals->addItem(signal_temp.name, i);
  }
  if (m_signals->getSignalCount() > 0) {
    ui->checkbox_enable_trigger->setEnabled(true);
  }
}

bool TriggerWidget::ifRestartTrigger() const {
  return m_settings.automaticRestart;
}

void TriggerWidget::setEnabled(bool enable) {
  m_settings.enabled = enable;
  emit settingsChanged(m_settings);
}

void TriggerWidget::triggerLevelChanged(double level) {
  m_settings.level = level;
  emit settingsChanged(m_settings);
}

void TriggerWidget::triggerTypeChanged(int type) {
  m_settings.type = static_cast<TriggerType>(type);
  emit settingsChanged(m_settings);
}

void TriggerWidget::signalIndexChanged(int index) {
  m_settings.signalIndex = index;
  emit settingsChanged(m_settings);
}

void TriggerWidget::triggerEnableChanged(bool checked) { setEnabled(checked); }

void TriggerWidget::triggerRestartChanged(bool checked) {
  m_settings.automaticRestart = checked;
  emit settingsChanged(m_settings);
}

void TriggerWidget::triggerTimeBeforeChanged(double time) {
  m_settings.timeBeforeTrigger = time;
  emit settingsChanged(m_settings);
}

void TriggerWidget::triggerTimeAfterChanged(double time) {
  m_settings.timeAfterTrigger = time;
  emit settingsChanged(m_settings);
}

void TriggerWidget::triggered() {
  triggerFinished();
  if (!m_settings.automaticRestart) {
    ui->checkbox_enable_trigger->setChecked(false);
    return;
  }
  ui->checkbox_enable_trigger->setChecked(true);
}

void TriggerWidget::disableTrigger() {
  setEnabled(false);
  m_settings.automaticRestart = false;
  ui->checkbox_restart_trigger->setChecked(false);
  ui->checkbox_enable_trigger->setChecked(false);
}

void TriggerWidget::newTriggerValue(double value) {
  ui->lbl_value->setText(QString::number(value));
}

void TriggerWidget::triggerFinished() {
  ui->lbl_status->setStyleSheet("QWidget { background-color: green }");
  ui->pb_start_trigger->setEnabled(true);
  ui->checkbox_enable_trigger->setEnabled(true);
}

void TriggerWidget::triggerStarted() {
  ui->lbl_status->setStyleSheet("QWidget { background-color: yellow }");
  ui->pb_start_trigger->setEnabled(false);
  ui->checkbox_enable_trigger->setEnabled(false);
}
