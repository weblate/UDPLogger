#ifndef EXPORTDATA_H
#define EXPORTDATA_H

#include <QThread>
#include "udp.h"
class Signals;

class ExportData : public QThread {
  Q_OBJECT
public:
  explicit ExportData(const QString& path, const QString& project_name,
                      QVector<struct udpMessageBuffer>& data, int index_first,
                      const uint64_t& index_last, int udp_buffer_size,
                      const Signals* signal, QObject* parent = nullptr);
  void run() override;

private:
  QVector<struct udpMessageBuffer> m_data;
  QString m_path;
  QString m_project_name;
  int m_first, m_last, m_udp_buffer_size;
  const Signals* m_signals;

signals:
  void resultReady();
  void showInfoMessage(QString title, QString text);

public slots:
};

#endif // EXPORTDATA_H
