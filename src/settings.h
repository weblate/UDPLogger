#ifndef SETTINGS_H
#define SETTINGS_H

#include <QSettings>

namespace SettingsKey {
const QString language = "language";
}

class Settings : public QSettings {
public:
  void setValue(const QString& key, const QVariant& value);
  QVariant value(const QString& key) const;
};

#endif // SETTINGS_H
