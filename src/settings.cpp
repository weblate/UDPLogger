#include "settings.h"

#include <QSettings>

namespace {
using namespace SettingsKey;
QHash<QString, QString> defaultSettings = {
    {language, "system"},
};
} // namespace

void Settings::setValue(const QString& key, const QVariant& value) {
  QSettings::setValue(key, value);
}

QVariant Settings::value(const QString& key) const {
  return QSettings::value(key, defaultSettings.value(key));
}
