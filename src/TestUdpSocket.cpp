#include "TestUdpSocket.h"

const int datagramsize = 20;

TestUdpSocket::TestUdpSocket(QObject* parent) : QObject(parent) {
  m_buffer = (char*)malloc(datagramsize);
}

qint64 TestUdpSocket::readDatagram(char* data, qint64 maxSize,
                                   QHostAddress* address, quint16* port) {
  assert(datagramsize < maxSize);
  for (int i = 0; i < datagramsize; i++) {
    data[i] = m_buffer[i];
  }

  return datagramsize;
}

bool TestUdpSocket::bind(const QHostAddress& hostaddress, quint16 port) {
  m_hostaddress = hostaddress;
  m_port = port;
  return m_bind_ok;
}

void TestUdpSocket::setTime(int32_t value) {
  time = value;
  createUDPPackage();
}

void TestUdpSocket::setTemp(float value) {
  temp = value;
  createUDPPackage();
}

void TestUdpSocket::setGyx(float value) {
  gyx = value;
  createUDPPackage();
}

void TestUdpSocket::setGyy(float value) {
  gyy = value;
  createUDPPackage();
}

void TestUdpSocket::setGyz(float value) {
  gyz = value;
  createUDPPackage();
}

int TestUdpSocket::createUDPPackage() {
  char* pointer = m_buffer;

  // If new variables are added increase also the number in the malloc!
  pointer = m_buffer + 0;
  memcpy(pointer, &time, sizeof(time));
  pointer = m_buffer + 4;
  memcpy(pointer, &temp, sizeof(temp));
  pointer = m_buffer + 8;
  memcpy(pointer, &gyx, sizeof(gyx));
  pointer = m_buffer + 12;
  memcpy(pointer, &gyy, sizeof(gyy));
  pointer = m_buffer + 16;
  memcpy(pointer, &gyz, sizeof(gyz));
  return 0;
}
