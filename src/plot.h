#ifndef PLOT_H
#define PLOT_H
/***
 *  This file is part of UDPLogger
 *
 *  Copyright (C) 2018 Martin Marmsoler, martin.marmsoler at gmail.com
 *
 *  UDPLogger is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with UDPLogger.  If not, see <http://www.gnu.org/licenses/>.
 ***/

#include <QMouseEvent>
#include "signals.h"
#include "qcustomplot.h"

class PlotBuffers;
class ChangeGraphDialog;
class PlotsContextMenu;

struct SignalSettings {
  QColor color;
  QCPGraph::LineStyle linestyle;
  QCPScatterStyle::ScatterShape scatterstyle;
  struct Signal signal_yaxis;
  struct Signal signal_xaxis;
  QString name;
};

struct PlotSettings {
  bool ifautomatic_range{true};
  bool ifrelative_ranging{false};
  double ymin{0}; // manual ranging
  double ymax{1}; // manual ranging
  double automatic_value{0};
  QVector<SignalSettings> signal_settings;
};

class Plot : public QCustomPlot {
  Q_OBJECT
public:
  Plot(int index, Signals*, PlotBuffers*, QWidget* parent);
  void mousePressEvent(QMouseEvent* ev);
  bool ifNameExists(QString name);
  void deleteGraph(const Signal& xaxis, const Signal& yaxis, int index);
  void writeJSON(QJsonObject& object);
  void importSettings(QJsonObject& object);

  ~Plot();
private slots:
  void ShowContextMenu(const QPoint& pos);
  void deletePlot();
  void clearPlot();
  void changeGraphStyle();
  void updateSettings(const PlotSettings& settings);
  void changePlot();
  void heightChanged(int height);
public slots:
  void newData();
  void newGraph(const SignalSettings& settings);
  void changeGraphSettings(int index_graph, const SignalSettings& new_settings,
                           bool remove_signal);
signals:
  void deletePlot2(Plot* plot_address);
  void removeSignal(const Signal& xaxis, const Signal& yaxis);
  void graphRemoved(const Signal& xaxis, const Signal& yaxis);

private:
  int m_index;
  PlotsContextMenu* m_context_menu;
  QMenu* m_menu;
  PlotSettings m_settings;
  Signals* m_signals;
  PlotBuffers* m_data_buffers;

  int m_plot_buffer_index;
  int m_plot_buffersize;
  int m_udp_buffersize;
  int buffer_index;

  friend class TestPlot;
  friend class TestPlots;
};

#endif // PLOT_H
