#ifndef GUISETTINGSDIALOG_H
#define GUISETTINGSDIALOG_H

#include <QDialog>

namespace Ui {
class GuiSettingsDialog;
}

class GuiSettingsDialog : public QDialog {
  Q_OBJECT

public:
  explicit GuiSettingsDialog(const QString& currLang,
                             QWidget* parent = nullptr);
  ~GuiSettingsDialog();
  QString language() const;

private:
  Ui::GuiSettingsDialog* ui;
};

#endif // GUISETTINGSDIALOG_H
