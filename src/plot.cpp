#include "plot.h"

/***
 *  This file is part of UDPLogger
 *
 *  Copyright (C) 2018 Martin Marmsoler, martin.marmsoler at gmail.com
 *
 *  UDPLogger is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with UDPLogger.  If not, see <http://www.gnu.org/licenses/>.
 ***/

#include <algorithm>
#include "plotscontextmenu.h"
#include "changegraphdialog.h"
#include "changediagramproperties.h"
#include "plotbuffer.h"

namespace JsonSections {
const QString graphs = "Graphs";

const QString min_height = "MinHeight";
const QString ymin = "yMin";
const QString ymax = "yMax";
const QString automatic_value = "AutomaticValue";
const QString if_automatic_range = "IfAutomaticRange";
const QString if_relative_range = "IfRelativeRange";

const QString graph_name = "GraphName";
const QString color = "Color";
const QString scatterStyle = "ScatterStyle";
const QString lineStyle = "LineStyle";

const QString signal_y_axis = "SignalYAxis";
const QString signal_x_axis = "SignalXAxis";

const QString datatype = "Datatype";
const QString signal_name = "SignalName";
const QString struct_name = "StructName";
const QString unit = "Unit";
} // namespace JsonSections

Plot::Plot(int index, Signals* signal, PlotBuffers* dataBuffers,
           QWidget* parent)
    : QCustomPlot(parent), m_index(index), m_signals(signal),
      m_data_buffers(dataBuffers) {

  m_context_menu = new PlotsContextMenu();
  m_context_menu->setContextMenuPolicy(Qt::CustomContextMenu);
  connect(m_context_menu, &PlotsContextMenu::customContextMenuRequested, this,
          &Plot::ShowContextMenu);

  // visible context menu
  m_menu = new QMenu;

  QAction* change_graphstyle = new QAction(tr("Change Graphstyle"), this);
  connect(change_graphstyle, &QAction::triggered, this,
          &Plot::changeGraphStyle);
  m_menu->addAction(change_graphstyle);

  QAction* add_graph = new QAction(tr("Add Graph"), this);
  connect(add_graph, &QAction::triggered, this, &Plot::changeGraphStyle);
  m_menu->addAction(add_graph);

  QAction* clear_plot = new QAction(tr("Change plot properties"), this);
  connect(clear_plot, &QAction::triggered, this, &Plot::changePlot);
  m_menu->addAction(clear_plot);

  QAction* delete_plot = new QAction(tr("Delete Plot"), this);
  connect(delete_plot, &QAction::triggered, this, &Plot::deletePlot);
  m_menu->addAction(delete_plot);

  m_plot_buffer_index = 0;

  legend->setVisible(true);

  setMinimumHeight(100);
}

void Plot::ShowContextMenu(const QPoint& pos) {
  qDebug() << "ShowContextMenu Position: " << pos;

  m_menu->exec(pos);
}

void Plot::deleteGraph(const Signal& xaxis, const Signal& yaxis, int index) {
  removeGraph(graph(index));

  emit graphRemoved(xaxis, yaxis);
  replot();
}

void Plot::newGraph(const SignalSettings& settings) {
  addGraph();
  changeGraphSettings(graphCount() - 1, settings, false);
  graph(graphCount() - 1)->setAdaptiveSampling(true);
}

void Plot::updateSettings(const PlotSettings& settings) {
  clearPlot();
  replot();
  m_settings = settings;

  // TODO: maybe instead of clearing the plot and creating all
  // plots again. Asign a unique hash and use it to modify/add/delete
  // plots
  for (auto& ss : m_settings.signal_settings) {
    newGraph(ss);
  }
  replot();
}

void Plot::changeGraphSettings(int index_graph,
                               const SignalSettings& new_settings,
                               bool remove_signal) {
  graph(index_graph)
      ->setLineStyle(static_cast<QCPGraph::LineStyle>(new_settings.linestyle));
  QCPScatterStyle::ScatterShape value =
      static_cast<QCPScatterStyle::ScatterShape>(new_settings.scatterstyle);
  graph(index_graph)->setScatterStyle(value);
  graph(index_graph)->setName(new_settings.name);
  graph(index_graph)->setPen(QPen(QColor(new_settings.color)));
  QSharedPointer<QCPGraphDataContainer> data_puffer = m_data_buffers->getBuffer(
      new_settings.signal_xaxis, new_settings.signal_yaxis);
  // problem: durch das ersetzen der alten daten, wird der alte shared pointer
  // gelöscht, aber der wurde ja bereits durch remove signals gelöscht
  graph(index_graph)->setData(data_puffer);
  graph(index_graph)
      ->setName(new_settings.name + " [" + new_settings.signal_yaxis.unit +
                "]");

  // Delete signals
  const auto& oldSignalsSettings = m_settings.signal_settings.at(index_graph);
  if (remove_signal &&
      new_settings.signal_xaxis.index !=
          oldSignalsSettings.signal_xaxis.index &&
      new_settings.signal_yaxis.index !=
          oldSignalsSettings.signal_yaxis.index) {
    removeSignal(oldSignalsSettings.signal_xaxis,
                 oldSignalsSettings.signal_yaxis);
  }
  replot();
}

bool Plot::ifNameExists(QString name) {
  for (int i = 0; i < plottableCount(); i++) {
    if (graph(i)->name().compare(name) == 0) {
      return 1;
    }
  }
  return 0;
}

void Plot::changeGraphStyle() {
  // change Graph Dialog
  ChangeGraphDialog dialog(m_signals, m_settings, parentWidget());
  Qt::WindowFlags flags = dialog.windowFlags();
  dialog.setWindowFlags(flags | Qt::Tool);

  connect(&dialog, &ChangeGraphDialog::settingsChanged, this,
          &Plot::updateSettings);
  dialog.exec();
}

void Plot::changePlot() {
  ChangeDiagramProperties window(this);
  window.setProperties(minimumHeight());
  connect(&window, &ChangeDiagramProperties::heightChanged, this,
          &Plot::heightChanged);
  window.exec();
}

void Plot::heightChanged(int height) { setMinimumHeight(height); }

void Plot::mousePressEvent(QMouseEvent* ev) {

  if (ev->button() == Qt::MouseButton::RightButton) {
    m_context_menu->customContextMenuRequested(ev->globalPos());
  }
}

void Plot::clearPlot() { clearGraphs(); }

void Plot::deletePlot() { deletePlot2(this); }

void Plot::newData() {
  // process new data

  bool range_found;
  if (graphCount() <= 0)
    return;

  double xmin, xmax;
  QCPRange range = graph(0)->getKeyRange(range_found);
  if (range_found) { // otherwise let old range
    xmin = range.lower;
    xmax = range.upper;
    xAxis->setRange(xmin, xmax);
  }

  // y Axis
  double ymax, ymin;
  bool ranges_found = false;
  if (m_settings.ifautomatic_range) {
    for (unsigned int i = 0; i < graphCount(); i++) {

      range = graph(i)->getValueRange(range_found);
      if (range_found) {
        if (ranges_found == false) { // first range which was found
          ymin = range.lower;
          ymax = range.upper;
          ranges_found = true;
          continue;
        }

        if (ymax < range.upper) {
          ymax = range.upper;
        }
        if (ymin > range.lower) {
          ymin = range.lower;
        }
      }
    }
    if (ranges_found) {
      if (m_settings.ifrelative_ranging) {
        double min_factor;
        double max_factor;
        if (ymin < 0) {
          min_factor = m_settings.automatic_value;
        } else if (ymin > 0) {
          min_factor = 1 - m_settings.automatic_value;
        } else {
          min_factor = 1;
        }

        if (ymax > 0) {
          max_factor = m_settings.automatic_value;
        } else if (ymax < 0) {
          max_factor = 1 - m_settings.automatic_value;
        } else {
          max_factor = 1;
        }

        yAxis->setRange(ymin * min_factor, ymax * max_factor);
      } else {
        if (ymin < 0) {
          ymin -= m_settings.automatic_value;
        } else { // ymin >=0
          ymin -= m_settings.automatic_value;
        }

        if (ymax >= 0) {
          ymax += m_settings.automatic_value;
        } else { // ymax <0
          ymax += m_settings.automatic_value;
        }

        yAxis->setRange(ymin, ymax);
      }
    }

  } else {
    yAxis->setRange(m_settings.ymin, m_settings.ymax);
  }

  replot();
  update();
}

void Plot::writeJSON(QJsonObject& object) {

  object[JsonSections::min_height] = minimumHeight();
  object[JsonSections::ymin] = m_settings.ymin;
  object[JsonSections::ymax] = m_settings.ymax;
  object[JsonSections::if_relative_range] = m_settings.ifrelative_ranging;
  object[JsonSections::if_automatic_range] = m_settings.ifautomatic_range;
  object[JsonSections::automatic_value] = m_settings.automatic_value;
  QJsonArray graphs;
  for (auto& signalsettings : m_settings.signal_settings) {
    QJsonObject graph;

    graph[JsonSections::color] = QString(signalsettings.color.name());
    qDebug() << "QColor to string" << QString(signalsettings.color.name());
    graph[JsonSections::lineStyle] = signalsettings.linestyle;
    graph[JsonSections::scatterStyle] = signalsettings.scatterstyle;
    graph[JsonSections::graph_name] = signalsettings.name;
    graph[JsonSections::signal_y_axis] = signalsettings.signal_yaxis.name;
    graph[JsonSections::signal_x_axis] = signalsettings.signal_xaxis.name;

    graphs.append(graph);
  }
  if (graphs.count() > 0) {
    object[JsonSections::graphs] = graphs;
  }
}

void Plot::importSettings(QJsonObject& plot_settings) {
  if (!plot_settings.contains(JsonSections::graphs)) {
    qDebug() << "No plot settings found";
    return;
  }
  setMinimumHeight(plot_settings[JsonSections::min_height].toInt());
  m_settings.ymin = plot_settings[JsonSections::ymin].toDouble();
  m_settings.ymax = plot_settings[JsonSections::ymax].toDouble();
  m_settings.automatic_value =
      plot_settings[JsonSections::automatic_value].toDouble();
  m_settings.ifautomatic_range =
      plot_settings[JsonSections::if_automatic_range].toBool();
  m_settings.ifrelative_ranging =
      plot_settings[JsonSections::if_relative_range].toBool();

  QJsonArray graphs = plot_settings[JsonSections::graphs].toArray();
  for (int j = 0; j < graphs.count(); j++) {

    QJsonObject graph = graphs[j].toObject();

    SignalSettings signal_settings;
    signal_settings.name = graph[JsonSections::graph_name].toString();
    signal_settings.color = QColor(graph[JsonSections::color].toString());
    signal_settings.scatterstyle = static_cast<QCPScatterStyle::ScatterShape>(
        graph[JsonSections::scatterStyle].toInt());
    signal_settings.linestyle = static_cast<QCPGraph::LineStyle>(
        graph[JsonSections::lineStyle].toInt());

    signal_settings.signal_yaxis =
        m_signals->getSignal(graph[JsonSections::signal_y_axis].toString());
    signal_settings.signal_xaxis =
        m_signals->getSignal(graph[JsonSections::signal_x_axis].toString());

    m_settings.signal_settings.append(signal_settings);
  }

  for (auto& signal_settings : m_settings.signal_settings)
    newGraph(signal_settings);
}

Plot::~Plot() {
  m_menu->close();

  for (const SignalSettings& signal_settings : m_settings.signal_settings) {
    removeSignal(signal_settings.signal_xaxis, signal_settings.signal_yaxis);
  }

  delete m_menu;
}
